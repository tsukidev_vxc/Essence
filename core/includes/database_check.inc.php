<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

if (isset($_POST['DBIP']) && isset($_POST['DBPORT']) && isset($_POST['DBUSER']) && isset($_POST['DBPW']) && isset($_POST['DBNAME'])) {
    $conn = new mysqli($_POST['DBIP'], $_POST['DBUSER'], $_POST['DBPW'], $_POST['DBNAME'], intval($_POST['DBPORT'])) or die('false');
    if ($conn) {
        die('true');
    }
} else {
    die('false');
}
