<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_POST['UserName'])) {
    $TempUserName = $_POST['UserName'];

    $stmt = $db->prepare('SELECT * FROM ec_users INNER JOIN ec_user_info ON ec_users.UserID = ec_user_info.UInfo_UserID WHERE User_Name = ? AND ec_user_info.UInfo_Key = \'rank\'');
    $stmt->bind_param('s', $TempUserName);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $db->query('DELETE FROM ec_user_info WHERE UInfo_UserID = ' . intval($row['UserID']) . ' AND UInfo_Key = \'rank\'');
        }
    }
}
