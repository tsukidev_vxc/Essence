<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

header('Content-Type: application/json');
if (isset($_POST['GroupSort']) && isset($_POST['GroupName']) && isset($_POST['GroupColor']) && isset($_POST['GroupHTML1']) && isset($_POST['GroupHTML2'])) {
    $TempGroupSort = $_POST['GroupSort'];
    $TempGroupName = $_POST['GroupName'];
    $TempGroupColor = $_POST['GroupColor'];
    $TempGroupHTML1 = $_POST['GroupHTML1'];
    $TempGroupHTML2 = $_POST['GroupHTML2'];

    $stmt = $db->prepare('SELECT * FROM ec_groups WHERE Group_Name = ?');
    $stmt->bind_param('s', $TempGroupName);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows <= 0) {
        $stmt = $db->prepare('INSERT INTO ec_groups (Group_Sort, Group_Name, Group_Color, Group_HTML, Group_HTML_2) VALUES (?,?,?,?,?)');
        $stmt->bind_param('sssss', $TempGroupSort, $TempGroupName, $TempGroupColor, $TempGroupHTML1, $TempGroupHTML2);
        $stmt->execute();
        die(json_encode(array(
            "success" => "true",
            "message" => "Die neue Gruppe wurde erfolgreich hinzugefügt."
        )));
    } else {
        $stmt = $db->prepare('UPDATE ec_groups SET Group_Sort = ?, Group_Color = ?, Group_HTML = ?, Group_HTML_2 = ? WHERE Group_Name = ?');
        $stmt->bind_param('sssss', $TempGroupSort, $TempGroupColor, $TempGroupHTML1, $TempGroupHTML2, $TempGroupName);
        $stmt->execute();
        die(json_encode(array(
            "success" => "true",
            "message" => "Die Gruppe wurde erfolgreich editiert."
        )));
    }
}
