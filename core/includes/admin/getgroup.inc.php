<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

header('Content-Type: application/json');
if (isset($_POST['GroupName']) || isset($_POST['GroupID'])) {
    if (isset($_POST['GroupName'])) {
        $TempGroupName = $_POST['GroupName'];
        $TempGroup = Group::getGroupByName($TempGroupName);
    } else {
        $TempGroupID = $_POST['GroupID'];
        $TempGroup = new Group(intval($TempGroupID));
    }

    $permissions = array();
    $users = array();

    $stmt = $db->prepare('SELECT * FROM ec_user_info INNER JOIN ec_users ON ec_user_info.UInfo_UserID = ec_users.UserID WHERE UInfo_Key = \'rank\' AND UInfo_Value = ?');
    $stmt->bind_param('s', $TempGroup->ID);
    $stmt->execute();
    $result = $stmt->get_result();

    while ($row = $result->fetch_assoc()) {
        array_push($users, $row['User_Name']);
    }

    $TempIsDisabled = in_array('*', $TempGroup->Perms);
    array_push($permissions, array("permission" => '*', "have" => $TempIsDisabled, "desc" => 'Administrator'));
    foreach (Group::getAvailablePermissions() as $key => $value) {
        if ($TempGroup->hasPermission($key)) {
            array_push($permissions, array("permission" => $key, "have" => true, "desc" => $value, "disabled" => $TempIsDisabled));
        } else {
            array_push($permissions, array("permission" => $key, "have" => false, "desc" => $value, "disabled" => $TempIsDisabled));
        }
    }
    die(json_encode(array(
        "Name" => $TempGroup->Name,
        "Color" => $TempGroup->Color,
        "HTML1" => $TempGroup->HTML_1,
        "HTML2" => $TempGroup->HTML_2,
        "permissions" => $permissions,
        "users" => $users
    )));
}
