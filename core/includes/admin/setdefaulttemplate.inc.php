<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

header('Content-Type: application/json');
if (isset($_POST['TemplateID'])) {
    $TemplateID = $_POST['TemplateID'];

    $stmt = $db->prepare('SELECT * FROM ec_templates WHERE Template_ID = ?');
    $stmt->bind_param('s', $TemplateID);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        $db->query('UPDATE ec_templates SET Template_Standard = 0');
        $stmt = $db->prepare('UPDATE ec_templates SET Template_Standard = 1 WHERE Template_ID = ?');
        $stmt->bind_param('s', $TemplateID);
        $stmt->execute();
        die(json_encode(array(
            "success" => "true"
        )));
    } else {
        die(json_encode(array(
            "success" => "false"
        )));
    }
}
