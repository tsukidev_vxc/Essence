<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_COOKIE['CleanUnique'])) {
    $UsedImages = array_values($config->get('image:upload'));
    for ($i = 0; $i < count($UsedImages); $i++) {
        $UsedImages[$i] = basename($UsedImages[$i]);
    }
    $dir = new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] . '/uploads/unique');
    foreach ($dir as $file) {
        if (!$file->isFile() || $file->isDot()) continue;
        if (in_array($file->getFilename(), $UsedImages)) continue;
        unlink($file->getPathname());
    }
    setcookie('CleanUnique', 'true', time() - 1800, '/');
    die('Nicht genutzte Bilder im Uploads Ordner löschen');
} else {
    setcookie('CleanUnique', 'true', time() + 10, '/');
    $_COOKIE['CleanUnique'] = "true";
    die('Wirklich?');
}
