<?php
require $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_POST['key_name']) && isset($_POST['value'])) {
    $value = $_POST['value'];
    $split = explode('.', $_POST['key_name']);
    if (is_array($config->get($split[0]))) {
        $array = $config->get($split[0]);
        if (($value == "true") || ($value == "false")) {
            $array[$split[1]] = $value == "true";
        } else if (is_int($value)) {
            $array[$split[1]] = intval($value);
        } else {
            $array[$split[1]] = $value;
        }
        echo $value;
        $config->set($split[0], $array);
    } else {
        $config->set($split[0], $value);
    }
}
