<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_POST['GroupID']) && isset($_POST['Permission']) && isset($_POST['Set'])) {
    $TempGroupID = $_POST['GroupID'];
    $Set = $_POST['Set'];
    $TempGroup = new Group(intval($TempGroupID));
    if ($Set == "true") {
        $TempGroup->addPermission($_POST['Permission']);
    } else {
        $TempGroup->removePermission($_POST['Permission']);
    }
}
