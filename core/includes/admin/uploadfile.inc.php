<?php
require $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

header('Content-Type: application/json');
if (isset($_FILES['files']) && isset($_POST['Ext'])) {
    $allowedExt = explode(',', $_POST['Ext']);
    $file_name_array = explode('.', $_FILES['files']['name'][0]);
    $file_tmp = $_FILES['files']['tmp_name'][0];
    $file_ext = strtolower(end($file_name_array));
    if (isset($_POST['SaveAs'])) {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
        $file = $path . $_POST['SaveAs'] . '.' . $file_ext;
        move_uploaded_file($file_tmp, $file);
        $normalizeFile = "/uploads/" . basename($file);
    } else {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/unique/';
        $file = $path . generateUniqueID() . '.' . $file_ext;
        move_uploaded_file($file_tmp, $file);
        $normalizeFile = "/uploads/unique/" . basename($file);
    }
    if (in_array($file_ext, $allowedExt)) {
        die(json_encode(array(
            "success" => "true",
            "message" => "$normalizeFile"
        )));
    } else {
        die(json_encode(array(
            "success" => "false",
            "message" => "Es sind nur folgende Dateiformate in diesem Feld erlaubt: " . implode(', ', $allowedExt)
        )));
    }
}

function generateUniqueID($length = 6)
{
    $chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvXxYyZz1234567890";
    $result = "";
    $minLength = 4;
    $maxLength = 10;
    for ($i = 0; $i < $length; $i++) {
        $randomLength = rand($minLength, $maxLength);
        for ($x = 0; $x < $randomLength; $x++) {
            $result .= $chars[rand(0, strlen($chars) - 1)];
        }
        $result .= "-";
    }
    return substr($result, 0, strlen($result) - 1);
}
