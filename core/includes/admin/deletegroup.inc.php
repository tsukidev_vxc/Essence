<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if($user->Login == "false"){
    die('<meta http-equiv="refresh" content="0;url=/">');
}

header('Content-Type: application/json');
if(isset($_POST['GID'])){
    $TempGroupID = $_POST['GID'];

    $stmt = $db->prepare('SELECT * FROM ec_groups WHERE Group_ID = ?');
    $stmt->bind_param('s', $TempGroupID);
    $stmt->execute();
    $result = $stmt->get_result();
    if($result->num_rows > 0){
        $stmt = $db->prepare('DELETE FROM ec_groups WHERE Group_ID = ?');
        $stmt->bind_param('s', $TempGroupID);
        $stmt->execute();
        $stmt = $db->prepare('DELETE FROM ec_perms WHERE Perm_Group = ?');
        $stmt->bind_param('s', $TempGroupID);
        $stmt->execute();
        die(json_encode(array(
            "success" => "true",
            "message" => "Diese Gruppe wurde erfolgreich gelöscht"
        )));
    } else {
        die(json_encode(array(
            "success" => "false",
            "message" => "Es existiert keine Gruppe mit diesem Namen."
        )));
    }
}
?>