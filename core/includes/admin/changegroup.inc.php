<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_POST['UserID']) && isset($_POST['Group'])) {
    $TempGroup = Group::getGroupByName($_POST['Group']);
    $TempUser = User::getUserByID(intval($_POST['UserID']));
    if (isset($TempGroup)) {
        $TempUser->Group = $TempGroup;
        $TempUser->setInfo('rank', $TempGroup->ID);
    }
}
