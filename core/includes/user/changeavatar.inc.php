<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if($user->Login == "false"){
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if(isset($_POST['UserID']) && isset($_POST['FileName'])){
    $TempUserID = $_POST['UserID'];
    $TempFileName = $_POST['FileName'];

    $TempUser = User::getUserByID($TempUserID);
    $TempUser->setInfo("avatar", $TempFileName);
}
