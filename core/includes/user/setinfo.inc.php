<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_POST['Key']) && isset($_POST['Value'])) {
    $TempKey = $_POST['Key'];
    $TempValue = $_POST['Value'];

    $user->setInfo($TempKey, $TempValue);
}
