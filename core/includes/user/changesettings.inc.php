<?php
include $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

if (isset($_POST['Password1']) && isset($_POST['Password2'])) {
    $TempPassword1 = $_POST['Password1'];
    $TempPassword2 = $_POST['Password2'];

    if ($TempPassword1 != $TempPassword2) {
        die('false');
    }

    if (isset($_POST['UserName'])) $user->UserName = $_POST['UserName'];
    if (isset($_POST['UserMail'])) $user->UserMail = $_POST['UserMail'];
    $user->UserPassword = md5($TempPassword1);
    $user->save();
}
