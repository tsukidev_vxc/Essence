<?php
require $_SERVER['DOCUMENT_ROOT'] . '/core/script_init.php';
if (isset($_POST['license'])) {
    $licenseChecker = new LicenseChecker($_POST['license']);
    $license = $licenseChecker->checkLicense();
    $config->set('license_owner', $license ? $licenseChecker->data['client'] : "undefined");
    $config->set('license', $licenseChecker->data['license']);
    header('Content-Type: application/json');
    die(json_encode($licenseChecker->data));
} else if (isset($_POST['DBIP']) && isset($_POST['DBPORT']) && isset($_POST['DBUSER']) && isset($_POST['DBPW']) && isset($_POST['DBNAME']) && is_int($_POST['DBPORT'])) {
    $database = $config->get('database');
    $database['ip'] = $_POST['DBIP'];
    $database['port'] = intval($_POST['DBPORT']);
    $database['user'] = $_POST['DBUSER'];
    $database['password'] = $_POST['DBPW'];
    $database['name'] = $_POST['DBNAME'];
    $config->set('database', $database);
} else if (isset($_POST['SITETITLE']) && isset($_POST['SITEKEYWORDS']) && isset($_POST['SITEDESCRIPTION'])) {
    $siteinformation = $config->get('siteinformation:input');
    $siteinformation['title'] = $_POST['SITETITLE'];
    $siteinformation['keywords'] = $_POST['SITEKEYWORDS'];
    $siteinformation['description'] = $_POST['SITEDESCRIPTION'];
    $config->set('siteinformation:input', $siteinformation);
} else if (isset($_POST['USERNAME']) && isset($_POST['USERMAIL']) && isset($_POST['PASSWORD1']) && isset($_POST['PASSWORD2'])) {
    $PasswordHash = md5($_POST['PASSWORD1']);
    if ($_POST['PASSWORD1'] != $_POST['PASSWORD2']) {
        die('false');
    }
    $db->generateDatabaseStructure();
    $stmt = $db->prepare('INSERT INTO ec_users (User_Name, User_Password, User_Email) VALUES (?,?,?)');
    $stmt->bind_param('sss', $_POST['USERNAME'], $PasswordHash, $_POST['USERMAIL']);
    $stmt->execute();
}
