<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

spl_autoload_register('ClassAutoLoad');
spl_autoload_register('ConfigAutoLoad');

function ClassAutoLoad($className) {
  $path = $_SERVER['DOCUMENT_ROOT']. '/core/classes/';
  $extension = ".class.php";
  $fullPath = $path . $className . $extension;

  if (!file_exists($fullPath)){
    return false;
  }

  include_once $fullPath;
}

function ConfigAutoLoad($className){
  $path = $_SERVER['DOCUMENT_ROOT']. "/core/configs/";
  $extension = ".class.php";
  $fullPath = $path . $className . $extension;

  if (!file_exists($fullPath)){
    return false;
  }

  include_once $fullPath;
}

 ?>
