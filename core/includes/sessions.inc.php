<?php
if (session_status() != PHP_SESSION_ACTIVE) session_start();
if (isset($_SESSION['User_Name']) && isset($_SESSION['User_Password'])) {
    $TempUserName = $_SESSION['User_Name'];
    $TempPassword = $_SESSION['User_Password'];
} else if (isset($_COOKIE['User_Name']) && isset($_COOKIE['User_Password'])) {
    $TempUserName = $_COOKIE['User_Name'];
    $TempPassword = $_COOKIE['User_Password'];
} else {
    $TempUserName = "";
    $TempPassword = "";
}

if (isset($TempUserName) && isset($TempPassword)) {
    $user = new User($TempUserName, $TempPassword);
    if (isset($smarty)) {
        $smarty->assign('user', $user);
    }
}
