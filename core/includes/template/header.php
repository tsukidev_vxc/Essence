<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="<?= $config->get('siteinformation:input')['description'] ?>">
<meta name="keywords" content="<?= $config->get('siteinformation:input')['keywords'] ?>">
<meta name="theme-color" content="#454545"/>
<title><?php echo str_replace('&amp;', '&', $page) . ' &bull; ' . $sitename; ?></title>

<link rel="shortcut icon" href="<?= $config->get('image:upload')['favicon'] ?>" type="image/x-icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<link href="/dependency/mdb/css/bootstrap.min.css" rel="stylesheet">
<link href="/dependency/mdb/css/mdb.min.css" rel="stylesheet">
<link href="/dependency/mdb/css/addons/flag.min.css" rel="stylesheet">
<link href="/dependency/mdb/css/style.css" rel="stylesheet">
<script type="text/javascript" src="/dependency/mdb/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="/dependency/mdb/js/popper.min.js"></script>
<script type="text/javascript" src="/dependency/mdb/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/dependency/mdb/js/mdb.min.js"></script>