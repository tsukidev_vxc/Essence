<?php
function startsWith($haystack, $needle) {
    return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
}
function endsWith($haystack, $needle) {
    return substr_compare($haystack, $needle, -strlen($needle)) === 0;
}
function removeString($string, $replace){
  return substr($string, 0, strpos($string, $replace));
}
 ?>
