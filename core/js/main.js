/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

/**
 * Number Input Field
 * Usage: Just create an empty <div> with the class 'input-number' and the contents will be generated
 */
$(document).ready(function () {
    $(this).find('.input-number').each(function () {
        var title = ($(this).text() != null ? $(this).text() : "");
        var data = $(this).attr('name');
        var value = ($(this).attr('min') != null ? $(this).attr('min') : "0");
        var min = ($(this).attr('min') != null ? "min=" + $(this).attr('min') : "");
        var max = ($(this).attr('max') != null ? "max=" + $(this).attr('max') : "");
        $(this).html('' +
            title +
            '<div class="input-group">' +
            '<button type="button" class="btn btn-danger m-0 px-3 input-group-prepend number-decrease"><i class="fas fa-minus"></i></button>' +
            '<input type="text" class="form-control text-center number-value" pattern="[0-9-]+" name="' + data + '" id="' + data + '" value="' + value + '" ' + min + ' ' + max + ' required>' +
            '<button type="button" class="btn btn-success m-0 px-3 input-group-prepend number-increase"><i class="fas fa-plus"></i></button>' +
            '</div>');
    });
    $(this).find('.number-value').inputFilter(function (value) {
        return /^\d*$/.test(value);
    });
    $(this).find('.input-number').on('contextmenu', function (e) {
        return false;
    });
    $(this).find('.number-decrease').on('click', function (e) {
        if ($(this).parent().find('.number-value').val().length <= 0)
            $(this).parent().find('.number-value').val($(this).parent().find('.number-value').attr('min'));
        let newValue = parseInt($(this).parent().find('.number-value').val()) - (e.ctrlKey && e.shiftKey ? 100 : e.ctrlKey || e.shiftKey ? 10 : 1);
        if (newValue < $(this).parent().find('.number-value').attr('min')) return;
        $(this).parent().find('.number-value').val(newValue);
    });
    $(this).find('.number-increase').on('click', function (e) {
        if ($(this).parent().find('.number-value').val().length <= 0)
            $(this).parent().find('.number-value').val($(this).parent().find('.number-value').attr('min'));
        let newValue = parseInt($(this).parent().find('.number-value').val()) + (e.ctrlKey && e.shiftKey ? 100 : e.ctrlKey || e.shiftKey ? 10 : 1);

        if (newValue > $(this).parent().find('.number-value').attr('max')) return;
        $(this).parent().find('.number-value').val(newValue);
    });
});

// Restricts input for the set of matched elements to the given inputFilter function.
(function ($) {
    $.fn.inputFilter = function (inputFilter) {
        return $(this).on("input keydown keyup mousedown mouseup select contextmenu drop", function (e) {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));