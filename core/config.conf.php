<?php
/***
*       _____             __ _                       _   _
*      / ____|           / _(_)                     | | (_)
*     | |     ___  _ __ | |_ _  __ _ _   _ _ __ __ _| |_ _  ___  _ __
*     | |    / _ \| '_ \|  _| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \
*     | |___| (_) | | | | | | | (_| | |_| | | | (_| | |_| | (_) | | | |
*      \_____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
*                               __/ |
*                              |___/
*                            © Essence 2020
*                        Coded by leVenour & Zoey
*/
return array (
  'license' => '',
  'license_owner' => '',
  'database' => 
  array (
    'ip' => '',
    'user' => '',
    'password' => '',
    'name' => '',
    'port' => ,
  ),
  'siteinformation:input' => 
  array (
    'title' => 'Essence',
    'description' => '',
    'keywords' => '',
  ),
  'sitesettings:switch' => 
  array (
    'maintenance' => 'true',
    'userregistration' => true,
    'profilepages' => false,
  ),
  'image:upload' => 
  array (
    'logo' => '/uploads/unique/FRxcddCy-7605iUA3-YUSsxM-AI8tALDho-Yjvq8jTGY-dzMi.png',
    'favicon' => '/uploads/unique/iOcJV19-p8cQr-2mkl-cexi5Jn-CYct48ecgA-7h1P.ico',
  ),
  'socialmedia:input' => 
  array (
    'twitter' => '',
    'youtube' => '',
    'email' => '',
  ),
  'navbaritems' => 
  array (
    'Home' => '/',
  ),
);
?>
