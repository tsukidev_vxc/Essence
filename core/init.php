<?php
/*
_____
|  ___|
| |__ ___ ___  ___ _ __   ___ ___
|  __/ __/ __|/ _ \ '_ \ / __/ _ \
| |__\__ \__ \  __/ | | | (_|  __/
\____/___/___/\___|_| |_|\___\___|

♡ Code by leVenour and Zoey ♡

➤ leVenour.at
➤ Zooeey.de
*/

/**
 * Global Veriables
 */
const WEBSITE_NAME = "Essence"; // Change Website Name
const VERSION = "0.1.1"; // Change Version
const AUTHORS = "leVenour & Zoey"; // Change Authors

// Does the Installer exist?
if (is_file($_SERVER['DOCUMENT_ROOT'] . '/pages/install.php')) {
    if (isset($_GET['from']) && $_GET['from'] == 'install') {
        rename($_SERVER['DOCUMENT_ROOT'] . '/pages/install.php', $_SERVER['DOCUMENT_ROOT'] . '/pages/1install.php');
        header('location: /');
        setcookie('UserName', 'aaa', time() - 1800, '/');
        setcookie('UserMail', 'aaa', time() - 1800, '/');
        setcookie('UserPassword', 'aaa', time() - 1800, '/');
        setcookie('install_step', 'aaa', time() - 1800, '/');
    } else {
        if (!($page == "Install")) {
            header('location: /install');
            die("Please run the Installer first!");
        }
    }
}

if (!isset($page)) {
    die('Page Loading Error!');
}

if (version_compare(phpversion(), '5.4', '<') && $page != "Install") {
    die('Please use at least PHP 5.4 or newer!');
}

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/core/config.php')) {
    if (!is_writable($_SERVER['DOCUMENT_ROOT'] . '/core')) {
        die('Your <strong>core</strong> directory is not writable, please check your file permissions.');
    }
}

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/cache/templates')) {
    try {
        mkdir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates', 0777, true);
    } catch (Exception $e) {
        die('Unable to create /cache directories, please check your file permissions.');
    }
}

// Start new session if not active
if (session_status() != PHP_SESSION_ACTIVE) session_start();

// Includes
require $_SERVER['DOCUMENT_ROOT'] . '/dependency/smarty/libs/Smarty.class.php';
include $_SERVER['DOCUMENT_ROOT'] . '/core/includes/autoloader.inc.php';

// Load Configs
$config = new CoreConfig;

// Load Classes
$smarty = new Smarty;
$lang = new Language($link);

/*Connections to the zooeey.de servers */
//$licenseChecker = new LicenseChecker($config->get('license'));
//$updateChecker = new UpdateChecker(WEBSITE_NAME);
//if (count($licenseChecker->data) <= 0) $licenseChecker->checkLicense();
//if (count($updateChecker->data) <= 0) $updateChecker->checkUpdate();


$db = new DB;

// Init Template System
if (!($page == "Install" || $page == "Admin")) {
    $smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'] . '/styles/templates/Default');
    $smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates/');
    $smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates/');
} else {
    $smarty->setTemplateDir($_SERVER['DOCUMENT_ROOT'] . '/core/templates/');
    $smarty->setCacheDir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates/');
    $smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates/');
}

// Initialize Sessions
require $_SERVER['DOCUMENT_ROOT'] . '/core/includes/sessions.inc.php';

// Add Language files
if (isset($_COOKIE['language']) && $lang->check_language($_COOKIE['language'])) {
    include $_SERVER['DOCUMENT_ROOT'] . '/styles/language/' . $_COOKIE['language'] . '/website.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/styles/language/' . $_COOKIE['language'] . '/installer.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/styles/language/' . $_COOKIE['language'] . '/admin.php';
} else {
    include $_SERVER['DOCUMENT_ROOT'] . '/styles/language/' . $lang->Default . '/website.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/styles/language/' . $lang->Default . '/installer.php';
    include $_SERVER['DOCUMENT_ROOT'] . '/styles/language/' . $lang->Default . '/admin.php';
}

// Assign Config Value to PHP Variable
$sitename = $config->get('siteinformation:input') != null ? $config->get('siteinformation:input')['title'] : "Essence";

// Assign Configvalues
foreach ($config->get('database') as $key => $val) {
    $smarty->assign('db_' . $key, $val);
}
foreach ($config->get('siteinformation:input') as $key => $val) {
    $smarty->assign('site_' . $key, $val);
}

$smarty->assign('config', $config->keys);


// Assign Language
if (!($page == "Install" || $page == "Admin")) {
    foreach ($language as $word => $value) {
        $smarty->assign($word, $value);
    }
} elseif ($page == "Install") {
    foreach ($language_install as $word => $value) {
        $smarty->assign($word, $value);
    }
} elseif ($page == "Admin") {
    foreach ($language_admin as $word => $value) {
        $smarty->assign($word, $value);
    }
} else {
    die('Language Files didnt Load correctly!');
}

$smarty->assign('available_languages', $lang->available_languages());
