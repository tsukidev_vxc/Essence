<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

class Language {

  public $Default = "DE";

  /*---------------------------------------------------------
  Lanuage Constructer
  Checks if the User Lanuage needs to be changed
  ---------------------------------------------------------*/
  function __construct($link) {
    if (isset($_GET['lang'])){
      if ($this->check_language($_GET['lang'])){
        if (isset($_GET['link'])){
          $link = $_GET['link'];
        }
        setcookie('language', $_GET['lang'], time() + (86400 * 3650), "/");
        header('location: ' . $link);
      }
    }
  }

  /*---------------------------------------------------------
  List all Available languages
  - Takes files out of the /styles/language folder
  - File must have the install.php, website.php, admin.php and lang_info.php files
  - If One of thoose does not exist the Langauge will be disabled
  ---------------------------------------------------------*/

  function available_languages(){
    $available_languages = array();

    $dir = new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] .'/styles/language');
    foreach ($dir as $fileinfo) {
      if ($fileinfo->isDir() && !$fileinfo->isDot()) {
        if ((file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/installer.php'))
        && (file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/website.php'))
        && (file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/admin.php'))
        && (file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/lang_info.php'))

      ) {
        include $_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/lang_info.php';
        array_push($available_languages, array('short' => $lang['short'],'long' => $lang['long'],'flag' => $lang['flag']));
      }
    }
  }
  return $available_languages;
}

function broken_languages(){
  $broken_languages = array();

  $dir = new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] .'/styles/language');
  foreach ($dir as $fileinfo) {
    if ($fileinfo->isDir() && !$fileinfo->isDot()) {
      if (!((file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/installer.php')))
      || (!(file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/website.php')))
      || (!(file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/admin.php')))
      || (!(file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/lang_info.php')))

    ) {
      array_push($broken_languages, array('short' => '/styles/langauge/' . $fileinfo->getFilename()));
    }
  }
}
return $broken_languages;
}


/*---------------------------------------------------------
Check if a Language File works
- Takes files out of the /styles/language folder
- File must have the install.php, website.php, admin.php and lang_info.php files
- If One of thoose does not exist the Langauge will be disabled
---------------------------------------------------------*/
function check_language($short){
  $dir = new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] .'/styles/language');
  foreach ($dir as $fileinfo) {
    if ($fileinfo->isDir() && !$fileinfo->isDot() && $fileinfo->getFilename() == $short) {
      if ((file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/installer.php'))
      && (file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/website.php'))
      && (file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/admin.php'))
      && (file_exists($_SERVER['DOCUMENT_ROOT'] .'/styles/language/' . $fileinfo->getFilename() . '/lang_info.php'))
    ) {
      return true;
    }
  }
}
}
}
?>
