<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

class UpdateChecker
{

    /**
     * Variables
     */
    public $url = "https://api.zooeey.de/updates/";
    public $website_sys;
    public $data = array();

    /**
     * Constructor
     */
    function __construct($website_sys)
    {
        $this->website_sys = $website_sys;
        $this->url = $this->url . $website_sys;
    }

    /**
     * Function to check if update servers are online
     * Return: true on success or false on failure
     */
    function isOnline(): bool
    {
        $curlInit = curl_init($this->url);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT_MS, 1000);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);
        if ($response) return true;
        return false;
    }

    /**
     * Function to check for a update
     * Return: true on success or false on failure
     */
    function checkUpdate(): bool
    {
        if ($this->isOnline()) {
            $data_keys = file_get_contents($this->url);
            $data = json_decode($data_keys, true);
            $this->data = $data;
            return version_compare($this->data['website_version'], VERSION, '==');
        } else {
            $this->data = array("website_name" => $this->website_sys, "website_version" => "undefined", "website_last_update" => "undefined");
            return false;
        }
    }
}
