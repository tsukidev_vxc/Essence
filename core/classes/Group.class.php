<?php

declare(strict_types=1);
class Group
{

    public static $DEFAULT_GROUP = "User";

    /**
     * Variables
     */
    public $ID = -1;
    public $Name = "User";
    public $Color = "gray";
    public $HTML_1 = "<a class=\"btn btn-gray btn-rounded btn-sm\">User</a>";
    public $HTML_2 = "<a class=\"btn btn-gray btn-rounded btn-sm\">User</a>";
    public $Perms = array();



    public function __construct(int $groupId)
    {
        global $db;
        if (isset($db)) {
            $stmt = $db->prepare("SELECT * FROM ec_groups WHERE Group_ID = ?");
            if ($stmt) {
                $stmt->bind_param('s', $groupId);
                $stmt->execute();
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $this->ID = intval($row['Group_ID']);
                        $this->Name = $row['Group_Name'];
                        $this->Color = $row['Group_Color'];
                        $this->HTML_1 = $row['Group_HTML'];
                        $this->HTML_2 = $row['Group_HTML_2'];
                        $stmt = $db->prepare("SELECT * FROM ec_perms WHERE Perm_Group = ?");
                        $stmt->bind_param('s', $this->ID);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                array_push($this->Perms, $row['Perm_Perml']);
                            }
                        }
                    }
                }
            }
        }
    }

    function togglePermission(string $toToggle)
    {
        if (in_array($toToggle, $this->Perms)) {
            $this->removePermission($toToggle);
        } else {
            $this->addPermission($toToggle);
        }
    }

    function addPermission(string $toAdd)
    {
        if ($this->ID == -1) return;
        global $db;
        if (!in_array($toAdd, $this->Perms)) {
            array_push($this->Perms, $toAdd);
            $stmt = $db->prepare('INSERT INTO ec_perms (Perm_Group, Perm_Perml) VALUES (?,?)');
            $stmt->bind_param('ss', $this->ID, $toAdd);
            $stmt->execute();
        }
    }

    function removePermission(string $toRemove)
    {
        if ($this->ID == -1) return;
        global $db;
        if (in_array($toRemove, $this->Perms)) {
            for ($i = 0; $i < count($this->Perms); $i++) {
                if ($this->Perms[$i] == $toRemove) {
                    unset($this->Perms[$i]);
                }
            }
            $stmt = $db->prepare('DELETE FROM ec_perms WHERE Perm_Group = ? AND Perm_Perml = ?');
            $stmt->bind_param('ss', $this->ID, $toRemove);
            $stmt->execute();
            $this->Perms = array_values($this->Perms);
        }
    }

    function hasPermission(string $toCheck)
    {
        return in_array($toCheck, $this->Perms) || in_array('*', $this->Perms);
    }

    public static function getGroupByName(string $groupName)
    {
        global $db;
        if (isset($db)) {
            $stmt = $db->prepare("SELECT * FROM ec_groups WHERE Group_Name = ?");
            if ($stmt) {
                $stmt->bind_param('s', $groupName);
                $stmt->execute();
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        return new Group(intval($row['Group_ID']));
                    }
                }
            }
        }
        return null;
    }

    public static function getAvailablePermissions()
    {
        global $db;
        $available_perms = array();
        $result = $db->query('SELECT * FROM ec_perms_list');
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $available_perms += array($row['Perml_Name'] => $row['Perml_Desc']);
            }
        }
        return $available_perms;
    }
}
