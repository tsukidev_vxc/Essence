<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

class Config {
    /**
     * Variables
     */
    private $path;
    private $fileName;
    public $keys;

    /**
     * Config constructor
     * @Param: The Path from root-dir, filename with the .php extenstion
     */
    function __construct($path, $fileName) {
        $this->path = $_SERVER['DOCUMENT_ROOT'] . '/' . $path;
        if (!file_exists($this->path)) {
            mkdir($this->path);
        }
        $this->fileName = $fileName;
        $this->write();
    }

    /**
     * Function to get an value from key
     * @Param: The key from value
     * @Return: The value or null if not exist
     */
    function get($key) {
        if (key_exists($key, $this->keys)) {
            return $this->keys[$key];
        }
        return null;
    }

    /**
     * Function to set an key in the config
     * @Param: The Key and the Value
     */
    function set($key, $value) {
        $this->keys[$key] = $value;
        $this->save();
    }

    /**
     * Function to remove an key from config
     * @Param: The key from value
     */
    function remove($key) {
        if (array_key_exists($key, $this->keys)) {
            unset($this->keys[$key]);
            $this->keys = array_values($this->keys);
            $this->save();
        }
    }

    /**
     * Function to write the default config or get the config keys
     */
    function write() {
        if (!file_exists($this->path . $this->fileName)) {
            $this->keys = array();
            if ($this instanceof DefaultCreation) {
                $this->keys = $this->onCreate();
            }
            $this->save();
            $this->keys = include $this->path . $this->fileName;
        } else {
            $this->keys = include $this->path . $this->fileName;
        }
    }

    /**
     * Function to save the current config with the current keys
     */
    function save() {
        file_put_contents($this->path . $this->fileName, "<?php
/***
*       _____             __ _                       _   _
*      / ____|           / _(_)                     | | (_)
*     | |     ___  _ __ | |_ _  __ _ _   _ _ __ __ _| |_ _  ___  _ __
*     | |    / _ \| '_ \|  _| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \
*     | |___| (_) | | | | | | | (_| | |_| | | | (_| | |_| | (_) | | | |
*      \_____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
*                               __/ |
*                              |___/
*                            © Essence 2020
*                        Coded by leVenour & Zoey
*/
return " . var_export($this->keys, true) . ";
?>");
        $this->write();
    }
}
