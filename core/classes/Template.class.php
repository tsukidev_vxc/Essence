<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

class Template {

    public $Default = "Default"; // GET Default Template from Database

    /*---------------------------------------------------------
    Lanuage Constructer
    Checks if the User Template needs to be changed
    ---------------------------------------------------------*/
    function __construct($route) {
        if (isset($_GET['template'])) {
            if ($this->check_template($_GET['template'])) {
                setcookie('template', $_GET['template'], time() + (86400 * 3650), "/");
                $page = $_SERVER['PHP_SELF'];
                header("Refresh: 0; url= /$route");
            }
        }
    }

    /*---------------------------------------------------------
    List all Available Template
    - Checks if the Template is activated in the Database
    ---------------------------------------------------------*/

    function available_template() {
        // Todo List all template from the database
    }
}

?>
