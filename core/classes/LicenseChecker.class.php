<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

class LicenseChecker
{

    /**
     * Variables
     */
    public $url = "http://api.zooeey.de/license/";
    public $license;
    public $data = array();

    /**
     * Constructor
     */
    function __construct($license)
    {
        $this->license = $license;
        $this->url = $this->url . $license;
    }

    /**
     * Function to check if license server are online
     * Return: true on success or false on failure
     */
    function isOnline(): bool
    {
        $curlInit = curl_init($this->url);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT_MS, 1000);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);
        if ($response) return true;
        return false;
    }

    /**
     * Function to check license
     * Return: true on success or false on failure
     */
    function checkLicense(): bool
    {
        if ($this->isOnline()) {
            $data_keys = file_get_contents($this->url);
            $data = json_decode($data_keys, true);
            if(!key_exists('client', $data)){
                $data['client'] = "undefined";
            }
            $this->data = $data;
            return $this->data['status'] == "true";
        } else {
            $this->data = array("status" => "true", "license" => $this->license, "client" => "undefined");
            return true;
        }
    }
}
