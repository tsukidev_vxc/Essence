<?php
/*
_____
|  ___|
| |__ ___ ___  ___ _ __   ___ ___
|  __/ __/ __ |/_ \ '_ \ / __/ _ \
| |__\__ \__ \  __/ | | | (_|  __/
\____/___/___/\___|_| |_|\___\___|

♡ Code by leVenour and Zoey ♡

➤ leVenour.at
➤ Zooeey.de
 */

declare(strict_types=1);
class User
{
    /**
     * Variables
     */
    public $UserID = -1;
    public $Login = "false";
    public $UserName = "undefined";
    public $UserMail = "undefined";
    public $UserPassword = "undefined";
    public $Group;
    public $Info = array();

    /**
     * Constructor
     */
    public function __construct(string $username, string $password = "")
    {
        global $db;
        if (isset($db)) {
            $stmt = $db->prepare("SELECT * FROM ec_users WHERE User_Name = ?");
            if ($stmt) {
                $stmt->bind_param('s', $username);
                $stmt->execute();
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $this->UserID = intval($row['UserID']);
                        $this->UserName = $row['User_Name'];
                        $this->UserMail = $row['User_Email'];
                        $this->UserPassword = $row['User_Password'];
                        if ($row['User_Password'] == $password) {
                            $this->Login = "true";
                        } else {
                            $this->Login = "false";
                        }
                        $stmt = $db->prepare("SELECT * FROM ec_user_info WHERE UInfo_UserID = ?");
                        $stmt->bind_param('s', $this->UserID);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                $this->Info += array($row['UInfo_Key'] => $row['UInfo_Value']);
                            }
                        }
                        if (isset($this->Info['rank'])) {
                            $this->Group = new Group(intval($this->Info['rank']));
                        } else {
                            $this->Group = Group::getGroupByName(Group::$DEFAULT_GROUP);
                        }
                    }
                }
            }
        }
    }

    public function setInfo(string $key, $value)
    {
        $this->Info[$key] = $value;
        $this->save();
    }

    public function getInfo(string $key)
    {
        if (isset($this->Info[$key])) {
            return $this->Info[$key];
        }
        return "";
    }

    public function getAvatar(){
        if (isset($this->Info['avatar'])) {
            return $this->Info['avatar'];
        }
        return "/uploads/essence/no_profile.png";
    }

    public function save()
    {
        global $db;
        if ($this->Login == "true") {
            $stmt = $db->prepare('UPDATE ec_users SET User_Name = ?, User_Password = ?, User_Email = ? WHERE UserID = ?');
            $stmt->bind_param('ssss', $this->UserName, $this->UserPassword, $this->UserMail, $this->UserID);
            $stmt->execute();
        } else {
            $stmt = $db->prepare('UPDATE ec_users SET User_Name = ?, User_Email = ? WHERE UserID = ?');
            $stmt->bind_param('sss', $this->UserName, $this->UserMail, $this->UserID);
            $stmt->execute();
        }
        $stmtUpdate = $db->prepare('UPDATE ec_user_info SET UInfo_Key = ?, UInfo_Value = ? WHERE UInfo_Key = ? AND UInfo_UserID = ?');
        $stmtInsert = $db->prepare('INSERT INTO ec_user_info (UInfo_UserID, UInfo_Desc, UInfo_Key, UInfo_Value) VALUES (?, ?, ?, ?)');
        foreach ($this->Info as $key => $value) {
            $result = $db->query('SELECT * FROM ec_user_info WHERE UInfo_Key = \'' . $key . '\' AND UInfo_UserID = \'' . $this->UserID . '\'');
            if ($result->num_rows > 0) {
                $stmtUpdate->bind_param('ssss', $key, $value, $key, $this->UserID);
                $stmtUpdate->execute();
            } else {
                $stmtInsert->bind_param('ssss', $this->UserID, $key, $key, $value);
                $stmtInsert->execute();
            }
        }
        $stmtInsert->close();
        $stmtUpdate->close();
    }

    public static function getUserByName(string $UserName)
    {
        global $db;
        if (isset($db)) {
            $stmt = $db->prepare("SELECT * FROM ec_users WHERE User_Name = ?");
            if ($stmt) {
                $stmt->bind_param('s', $UserName);
                $stmt->execute();
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    return new User($UserName);
                }
            }
        }
        return null;
    }

    public static function getUserByID(int $UserID)
    {
        global $db;
        if (isset($db)) {
            $stmt = $db->prepare("SELECT * FROM ec_users WHERE UserID = ?");
            if ($stmt) {
                $stmt->bind_param('s', $UserID);
                $stmt->execute();
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        return new User($row['User_Name']);
                    }
                }
            }
        }
        return null;
    }

    public static function exist(string $username, string $password): bool
    {
        global $db;
        $stmt = $db->prepare("SELECT * FROM ec_users WHERE User_Name = ? AND User_Password = ?");
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows > 0;
    }
}
