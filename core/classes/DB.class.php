<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

class DB
{

    /**
     * Database Constructor
     */
    function __construct()
    {
        $config = new CoreConfig;
        if ($config->get('database') != null) {
            $database = $config->get('database');
            $this->conn = new mysqli($database['ip'], $database['user'], $database['password'], null, $database['port']) or die();
            if ($this->conn) {
                $this->query('USE ' . $database['name']);
                if ($this->query('SELECT * FROM ec_groups')) {
                    $result = $this->query('SELECT * FROM ec_groups WHERE Group_Name = \''.Group::$DEFAULT_GROUP.'\'');
                    if($result->num_rows <= 0){
                        $this->query('INSERT INTO ec_groups (Group_Sort, Group_Name, Group_Color, Group_HTML, Group_HTML_2) VALUES (999, \''.Group::$DEFAULT_GROUP.'\',\'gray\',\'<a class="btn btn-elegant btn-sm">User</a>\',\'<a class="btn btn-elegant-color-dark btn-sm">User</a>\')');
                    }
                }
            }
        }
    }

    /**
     * Function to create an normal database query
     * @Param: The database query
     * @Return: mysqli_result
     */
    function query($qry)
    {
        return $this->conn->query($qry);
    }

    /**
     * Function to create an prepared stamement
     * @Param: The database query
     * @Return: mysqli_stmt
     */
    function prepare($qry)
    {
        return $this->conn->prepare($qry);
    }

    public function checkDatabase(): bool
    {
        if (isset($this->conn) && $this->conn) {
            return $this->query('SELECT * FROM ec_users') && $this->query('SELECT * FROM ec_user_info') && $this->query('SELECT * FROM ec_groups')
                && $this->query('SELECT * FROM ec_perms_list') && $this->query('SELECT * FROM ec_perms') && $this->query('SELECT * FROM ec_templates')
                && $this->query('SELECT * FROM ec_uploads') && $this->query('SELECT * FROM ec_language') && $this->query('SELECT * FROM ec_route')
                && $this->query('SELECT * FROM ec_custom_page') && $this->query('SELECT * FROM ec_addons') && $this->query('SELECT * FROM ec_user_bans');
        } else {
            return false;
        }
    }

    /**
     * Function to generate the database structure
     */
    function generateDatabaseStructure()
    {
        if (!$this->query('SELECT * FROM ec_users')) {
            $this->query('CREATE TABLE `ec_users` (
        `UserID` int(11) NOT NULL,
        `User_Name` varchar(255) NOT NULL,
        `User_Password` varchar(255) NOT NULL,
        `User_Email` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_users` ADD PRIMARY KEY (`UserID`);');
            $this->query('ALTER TABLE `ec_users` MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_user_info')) {
            $this->query('CREATE TABLE `ec_user_info` (
        `UInfo_ID` int(11) NOT NULL,
        `UInfo_UserID` int(11) NOT NULL,
        `UInfo_Desc` varchar(255) NOT NULL,
        `UInfo_Key` varchar(255) NOT NULL,
        `UInfo_Value` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_user_info` ADD PRIMARY KEY (`UInfo_ID`);');
            $this->query('ALTER TABLE `ec_user_info` MODIFY `UInfo_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_groups')) {
            $this->query('CREATE TABLE `ec_groups` (
        `Group_ID` int(11) NOT NULL,
        `Group_Sort` int(11) NOT NULL,
        `Group_Name` varchar(255) NOT NULL,
        `Group_Color` varchar(255) NOT NULL,
        `Group_HTML` longtext NOT NULL,
        `Group_HTML_2` longtext NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_groups` ADD PRIMARY KEY (`Group_ID`);');
            $this->query('ALTER TABLE `ec_groups` MODIFY `Group_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_perms_list')) {
            $this->query('CREATE TABLE `ec_perms_list` (
        `Perml_ID` int(11) NOT NULL,
        `Perml_Name` varchar(255) NOT NULL,
        `Perml_Desc` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_perms_list` ADD PRIMARY KEY (`Perml_ID`);');
            $this->query('ALTER TABLE `ec_perms_list` MODIFY `Perml_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_perms')) {
            $this->query('CREATE TABLE `ec_perms` (
        `Perm_ID` int(11) NOT NULL,
        `Perm_Group` varchar(255) NOT NULL,
        `Perm_Perml` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_perms` ADD PRIMARY KEY (`Perm_ID`);');
            $this->query('ALTER TABLE `ec_perms` MODIFY `Perm_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_templates')) {
            $this->query('CREATE TABLE `ec_templates` (
        `Template_ID` int(11) NOT NULL,
        `Template_Name` varchar(255) NOT NULL,
        `Template_Preview` varchar(255) NOT NULL,
        `Template_Color` varchar(255) NOT NULL,
        `Template_Active` tinyint(1) NOT NULL,
        `Template_Standard` tinyint(1) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_templates` ADD PRIMARY KEY (`Template_ID`);');
            $this->query('ALTER TABLE `ec_templates` MODIFY `Template_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_uploads')) {
            $this->query('CREATE TABLE `ec_uploads` (
        `Upload_ID` int(11) NOT NULL,
        `Upload_File_Link` varchar(255) NOT NULL,
        `Upload_User_ID` int(11) NOT NULL,
        `Upload_IP` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_uploads` ADD PRIMARY KEY (`Upload_ID`);');
            $this->query('ALTER TABLE `ec_uploads` MODIFY `Upload_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_language')) {
            $this->query('CREATE TABLE `ec_language` (
        `Language_ID` int(11) NOT NULL,
        `Language_Name` varchar(255) NOT NULL,
        `Language_Link` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_language` ADD PRIMARY KEY (`Language_ID`);');
            $this->query('ALTER TABLE `ec_language` MODIFY `Language_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_route')) {
            $this->query('CREATE TABLE `ec_route` (
        `Route_ID` int(11) NOT NULL,
        `Route_User` varchar(255) NOT NULL,
        `Route_Link` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_route` ADD PRIMARY KEY (`Route_ID`);');
            $this->query('ALTER TABLE `ec_route` MODIFY `Route_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
        if (!$this->query('SELECT * FROM ec_user_bans')) {
            $this->query('CREATE TABLE `ec_user_bans` (
        `Ban_ID` int(11) NOT NULL,
        `Ban_UserID` int(11) NOT NULL,
        `Ban_Reason` varchar(255) NOT NULL,
        `Ban_End` bigint NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
            $this->query('ALTER TABLE `ec_user_bans` ADD PRIMARY KEY (`Ban_ID`);');
            $this->query('ALTER TABLE `ec_user_bans` MODIFY `Ban_ID` int(11) NOT NULL AUTO_INCREMENT;');
        }
    }
}
