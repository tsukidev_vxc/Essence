<?php

class CoreConfig extends Config implements DefaultCreation {
    function __construct() {
        parent::__construct('core/', 'config.conf.php');
    }

    function onCreate(): array {
        return array(
            'license' => '',
            'license_owner' => '',
            'database' => array(
                'ip' => 'localhost',
                'user' => 'root',
                'password' => '',
                'name' => 'essence',
                'port' => 3306
            ),
            'siteinformation:input' => array(
                'title' => 'Essence',
                'description' => '',
                'keywords' => ''
            ),
            'sitesettings:switch' => array(
                "maintenance" => "true",
                "userregistration" => "true",
                "profilepages" => "true"
            ),
            'image:upload' => array(
                "logo" => "",
                "favicon" => ""
            ),
            "socialmedia:input" => array(
                "twitter" => "",
                "youtube" => "",
                "email" => ""
            ),
            'navbaritems' => array(
                'Home' => '/'
            )
        );
    }
}
