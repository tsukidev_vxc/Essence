<?php
/*
_____
|  ___|
| |__ ___ ___  ___ _ __   ___ ___
|  __/ __/ __|/ _ \ '_ \ / __/ _ \
| |__\__ \__ \  __/ | | | (_|  __/
\____/___/___/\___|_| |_|\___\___|

♡ Code by leVenour and Zoey ♡

➤ leVenour.at
➤ Zooeey.de
*/

if (version_compare(phpversion(), '5.4', '<') && $page != "Install") {
  die('Please use at least PHP 5.4 or newer!');
}

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/core/config.php')) {
  if (!is_writable($_SERVER['DOCUMENT_ROOT'] . '/core')) {
    die('Your <strong>core</strong> directory is not writable, please check your file permissions.');
  }
}

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/cache/templates')) {
  try {
    mkdir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates', 0777, true);
  } catch (Exception $e) {
    die('Unable to create /cache directories, please check your file permissions.');
  }
}

// Start new session if not active
if (session_status() != PHP_SESSION_ACTIVE) session_start();

// Includes
include $_SERVER['DOCUMENT_ROOT'] .'/core/includes/autoloader.inc.php';

// Load Configs
$config = new CoreConfig;

// Load Classes
$db = new DB;

// Initialize Sessions
require $_SERVER['DOCUMENT_ROOT'] .'/core/includes/sessions.inc.php';