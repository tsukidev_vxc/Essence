<script type="text/javascript" src="../dependency/mdb/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../dependency/mdb/js/popper.min.js"></script>
<script type="text/javascript" src="../dependency/mdb/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../dependency/mdb/js/mdb.min.js"></script>

<script src="https://kit.fontawesome.com/4e8245fcbb.js"></script>
<script>
    var keySequenceLength = 0;
    $('[name=license]').on('input', function (e) {
        var maxLen = 14;
        $(this).val(convertLicenseKeyFromString($(this).val().substr(0, maxLen), 4));
    });
    $('#CheckKey_form').submit(function (e) {
        e.preventDefault();
        var licenseKey = $('[name=license]').val().replace(" ", "");
        $.post('/core/includes/install_scripts.inc.php', {
            license: licenseKey
        }).then(function (response) {
            var json = JSON.parse(JSON.stringify(response));
            if ($('#compatibleversion').hasClass('green-text') && $('#configwriteable').hasClass('green-text') && $('#createdcachefile').hasClass('green-text')) {
                if (json.status == "true") {
                    document.cookie = "install_step=database; max-age=" + 60 * 60 * 24 + "; path=/;";
                    location.href = "install";
                } else {
                    $('#CheckLicenseResult').html('{$licence_key_error}');
                    $('#CheckLicenseResult').css('color', 'red');
                }
            } else {
                $('#CheckLicenseResult').html('{$requirements_not_met}');
                $('#CheckLicenseResult').css('color', 'red');
            }
        })
    });
    $('#Database_form').submit(function (e) {
        e.preventDefault();
        var ip = $('[name=IP]').val();
        var port = $('[name=Port]').val();
        var user = $('[name=UserName]').val();
        var password = $('[name=Password]').val();
        var dbname = $('[name=DBName]').val();
        $.post('/core/includes/database_check.inc.php', {
            DBIP: ip,
            DBPORT: port,
            DBUSER: user,
            DBPW: password,
            DBNAME: dbname
        }).then(function (response) {
            if (response == "true") {
                $.post('/core/includes/install_scripts.inc.php', {
                    DBIP: ip,
                    DBPORT: port,
                    DBUSER: user,
                    DBPW: password,
                    DBNAME: dbname
                }).then(function (response) {
                    document.cookie = "install_step=site_information; max-age=" + 60 * 60 * 24 + "; path=/;";
                    location.href = "install";
                });
            } else {
                $('#CheckDatabaseResult').html('{$database_error}');
                $('#CheckDatabaseResult').css('color', 'red');
            }
        });
    });
    $('#Siteinfo_form').submit(function (e) {
        e.preventDefault();
        var title = $('[name=Title]').val();
        var keywords = $('[name=Keywords]').val();
        var description = $('[name=Description]').val();
        $.post('/core/includes/install_scripts.inc.php', {
            SITETITLE: title,
            SITEKEYWORDS: keywords,
            SITEDESCRIPTION: description
        }).then(function (response) {
            document.cookie = "install_step=account; max-age=" + 60 * 60 * 24 + "; path=/;";
            location.href = "install";
        });
    });
    $('#Account_form').submit(function (e) {
        e.preventDefault();
        var username = $('[name=UserName]').val();
        var usermail = $('[name=UserMail]').val();
        var password1 = $('[name=Password1]').val();
        var password2 = $('[name=Password2]').val();
        if (password1 != password2) {
            $('#AccountResult').html('Die Passwörter stimmen nicht überein.');
            $('#AccountResult').css('color', 'red');
            return;
        }
        $.post('/core/includes/install_scripts.inc.php', {
            USERNAME: username,
            USERMAIL: usermail,
            PASSWORD1: password1,
            PASSWORD2: password2
        }).then(function (response) {
            if (response == "false") {
                return;
            }
            location.href = "?from=install";
        });
    });

    function convertLicenseKeyFromString(input, length) {
        var output = "";
        var j = 0;
        for (var i = input.length - 1; i >= 0; --i) {
            if (input[i] == '-') continue;
            output = input[i] + output;
            if ((++j) % length == 0) {
                output = "-" + output;
            }
        }
        var result = (output[0] == '-' ? output.substr(1) : output);
        return result;
    }
</script>