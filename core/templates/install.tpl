<div class="container px-5 mt-5">
  <div class="card card-cascade wider">
    <div class="view view-cascade gradient-card-header text-center ">
      <h1 class="mb-3 title-text">Essence</h1>
    </div>
    <div class="card-body card-body-cascade">
      {if !$step == ""}
      <div class="text-center">
        <a>{$license}: <b>{$key}</b></a></br>
        <a>{$license_owner}: <b>{$key_for}</b></a>
      </div>
      <hr>
      {/if}
      <!-- Begin Step Display -->
      {if $step == ""}
      <!-- Display Welcome Page -->
      <h4 class="text-center">{$welcome_title}</h4>
      <hr>
      <div class="text-center">
        {$welcome_text}
        {if !$LicenseServerOnline}<h6 class="red-text">{$license_server_offline_text}</h6>{/if}
      </div>
      <hr>
      <div class="row">
        <div class="col-md-3">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td class="text-right p-1">{$php_version}: {$PHPVersion}</td>
                <td class="p-1">{if $CompatiblePHPVersion}<i id="compatibleversion" class="fas fa-check green-text"></i>
                  {else}<i id="compatibleversion" class="fas fa-times red-text"></i>{/if}</td>
              </tr>
              <tr>
                <td class="text-right p-1">{$write_perms}</td>
                <td class="p-1">{if $ConfigWritable}<i id="configwriteable" class="fas fa-check green-text"></i>
                  {else}<i id="configwriteable" class="fas fa-times red-text"></i> {/if}</td>
              </tr>
              <tr>
                <td class="text-right p-1">{$cache_write_perms}</td>
                <td class="p-1">{if $CreatedCacheFile}<i id="createdcachefile" class="fas fa-check green-text"></i>
                  {else}<i id="createdcachefile" class="fas fa-times red-text"></i>{/if}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-8 mt-2">
          <form id="CheckKey_form" method="post">
            <div class="input-group">
              <input name="license" type="text" class="form-control" placeholder="{$licence_key_name}">
              <div class="input-group-append">
                <button class="btn btn-primary m-0 px-3 py-2" type="submit">{$next_step}</button>
              </div>
              <br>
            </div>
            <h6 class="text-center"><span id="CheckLicenseResult"></span></h6>
          </form>
        </div>
      </div>

      {elseif $step == "database"}
      <!-- Display Database Setup -->
      <div class="text-center">
        <p>{$database_text}</p>
        <hr>
        <h5>{$database_title}</h5><br>
        <form id="Database_form" method="POST">
          <div class="row">
            <div class="col-lg">
              <input required type="text" name="IP" class="form-control" placeholder="{$database_ip}" value="{$db_ip}">
            </div>
            <br>
            <div class="col-lg">
              <input required type="text" name="Port" class="form-control" placeholder="{$database_port}"
                value="{$db_port}">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg">
              <input required type="text" name="UserName" class="form-control" placeholder="{$database_username}"
                value="{$db_user}">
            </div>
            <br>
            <div class="col-lg">
              <input type="password" name="Password" class="form-control" placeholder="{$database_password}">
            </div>
          </div>
          <br>
          <input required type="text" name="DBName" class="form-control" placeholder="{$database_name}"
            value="{$db_name}">
          <br>

          <h6 class="text-center"><span id="CheckDatabaseResult"></span></h6>
          <button type="submit" class="btn btn-primary btn-rounded capitialize">{$next_step}</button>
        </form>
      </div>

      {elseif $step == "site_information"}
      <!-- Display Site Setup -->
      <div class="text-center">
        <p>{$siteinformation_text}</p>
        <hr>
        <h5>{$siteinformation_titletext}</h5><br>
        <form id="Siteinfo_form" method="POST">
          <div class="row">
            <div class="col-lg">
              <input required type="text" name="Title" class="form-control" placeholder="{$siteinformation_title}"
                value="{$site_title}">
            </div>
            <br>
            <div class="col-lg">
              <input required type="text" name="Keywords" class="form-control" placeholder="{$siteinformation_keywords}"
                value="{$site_keywords}">
            </div>
          </div>
          <br>
          <textarea required type="text" name="Description" class="form-control md-textarea"
            placeholder="{$siteinformation_description}">{$site_description}</textarea>
          <br>

          <h6 class="text-center"><span id="SiteInfoeResult"></span></h6>
          <button type="submit" class="btn btn-primary btn-rounded capitialize">{$next_step}</button>
        </form>
        <!-- Close If Statement -->
      </div>

      {elseif $step == "account"}
      <!-- Display Site Setup -->
      <div class="text-center">
        <p>{$account_text}</p>
        <hr>
        <h5>{$account_titletext}</h5><br>
        <form id="Account_form" method="POST">
          <div class="row">
            <div class="col-lg">
              <input required type="text" name="UserName" class="form-control" placeholder="{$account_username}">
            </div>
            <br>
            <div class="col-lg">
              <input required type="email" name="UserMail" class="form-control" placeholder="{$account_email}">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg">
              <input required type="password" name="Password1" class="form-control" placeholder="{$account_password}">
            </div>
            <br>
            <div class="col-lg">
              <input required type="password" name="Password2" class="form-control"
                placeholder="{$account_passwordrepeat}">
            </div>
          </div>
          <br>
          <h6 class="text-center"><span id="AccountResult"></span></h6>
          <button type="submit" class="btn btn-primary btn-rounded capitialize">{$next_step}</button>
        </form>
        <!-- Close If Statement -->
      </div>
      {/if}
    </div>
  </div>
  <div class="card card-cascade wider">
    <div style="margin-right: 3.5%; margin-left: 3.5%;">
      <button class="btn btn-primary btn-sm dropdown-toggle mr-4" type="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">Language</button>

      <div class="dropdown-menu">
        {foreach from=$available_languages key=myId item=i}
        <a href="?lang={$i.short}" class="dropdown-item" href="?lang=DE">{$i.flag} {$i.long}</a>
        {/foreach}
      </div>
      <h6 class="float-right white-text mt-2">&copy; Essence 2020 &bull; Designed by Zoey & leVenour</h6>
    </div>
  </div>
</div>
{include file="scripts/install_scripts.tpl"}
