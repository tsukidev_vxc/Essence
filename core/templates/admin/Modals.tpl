<div class="modal fade" id="perms_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><span id="perms_groupname"></span> - Perms</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="perms_content"></div>
                <br>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="groupmembers_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-avatar" role="document">
        <div class="modal-content p-1">
            <h4 class="modal-title w-100 text-center" id="myModalLabel"><span id="groupmembers_groupname"></span> -
                Mitglieder
            </h4>
            <div class="modal-body">
                <div id="groupmembers_content"></div>
                <br>
                <button type="button" class="white-text btn admin-bg-red btn-sm btn-rounded"
                        data-dismiss="modal">{$close}</button>
            </div>
        </div>
    </div>
</div>