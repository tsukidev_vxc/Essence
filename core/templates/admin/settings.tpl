<h1>{$settings}</h1>
<hr>
<div class="row">
  {foreach $config as $key => $value}
  {if $key == "database" || $key == "navbaritems"}{continue}{/if}
  {if is_array($value)}
  <div class="col-lg-4">
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        <a class="text-capitalize">{removeString("$key", ":")}</a>
      </div>
      <div class="card-body">
        <div class="list-group list-group-flush">
          {foreach $value as $k => $v}
          <div class="input-group mt-3">
            {if endsWith($key , ":switch")}
            <div class="switch">
              <label>
                <a class="capitialize pl-1">{$k}:</a>
                <input name="Change{$key}.{$k}" type="checkbox" {if $v==true}checked{/if}>
                <span class="lever"></span>
              </label>
            </div>
            <script>
              $('[name="Change{$key}.{$k}"]').on('change', function (e) {
                toastr.success('Einstellungen gespeichert');
                e.preventDefault();
                var checked = $(this).is(':checked');
                setConfig('{$key}.{$k}', Boolean(checked));
              })
            </script>
            {elseif endsWith($key , ":input")}
            <div class="input-group-prepend" >
              <span class="input-group-text capitialize" id="inputGroup-sizing-lg">{$k}:</span>
            </div>
            <input if="{$key}.{$k}" name="Change{$key}.{$k}" type="text" class="form-control form-lg form-control-lg" placeholder="{$k}"
              value="{$v}">
            <script>
              $('[name="Change{$key}.{$k}"]').on('change', function (e) {
                toastr.success('Einstellungen gespeichert');
                e.preventDefault();
                var value = $(this).val();
                setConfig('{$key}.{$k}', value);
              })
            </script>
            {elseif endsWith($key , ":textarea")}

            <textarea name="Change{$key}.{$k}" class="md-textarea" placeholder="{$k}">{$v}</textarea>
            <script>
              $('[name="Change{$key}.{$k}"]').on('change', function (e) {
                toastr.success('Einstellungen gespeichert');
                e.preventDefault();
                var value = $(this).val();
                setConfig('{$key}.{$k}', value);
              })
            </script>
            {elseif endsWith($key , ":upload")}
            <div class="input-group">
              <div class="input-group-prepend">
                <span onclick="window.open('{$v}')" class="input-group-text" id="img{$key}.{$k}"
                  style="cursor: pointer;">
                  <img height="20px" src="{$v}" onerror="this.src='/uploads/essence/no_image.png';"></img>
                </span>
              </div>
              <div class="custom-file">
                <input id="Change{$key}.{$k}" type="file" name="Change{$key}.{$k}" class="custom-file-input admin-bg">
                {if $k != ""}
                <label class="custom-file-label overflow-hidden capitialize" for="Change{$key}.{$k}">
                  {$k}
                </label>
                {else}
                <label class="custom-file-label" for="Change{$key}.{$k}">Choose File</label>
                {/if}
              </div>
            </div>
            <script>
              $('[name="Change{$key}.{$k}"]').on('change', function (e) {
                e.preventDefault();
                const files = document.querySelector('[name="Change{$key}.{$k}"]').files;
                const formData = new FormData();
                for (let i = 0; i < files.length; i++) {
                  let file = files[i];
                  formData.append('files[]', file);
                }
                formData.append('Ext', "png,jpg,bmp,gif,ico");
                fetch('/core/includes/admin/uploadfile.inc.php', {
                  method: 'POST',
                  body: formData
                }).then((response) => {
                  return response.json();
                }).then((jsonResponse) => {
                  var json = JSON.parse(JSON.stringify(jsonResponse));
                  if (json.success == "true") {
                    toastr.success('Einstellung geändert');
                    $('#img{$key}.{$k}').html("<img height=\"20px\" src=\"" + json.message + "\" onerror=\"this.src='/uploads/essence/no_image.png';\"></img>");
                    setConfig('{$key}.{$k}', json.message);
                  } else {
                    toastr.error('' + json.message);
                  }
                });
              });
            </script>
            {/if}
          </div>
          <br>
          {/foreach}
        </div>
      </div>
    </div>
  </div>
  {/if}
  {/foreach}
  <div class="col-lg-4">
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        <a class="text-capitalize">Other Settings</a>
      </div>
      <div class="card-body">
        <div class="list-group list-group-flush">
          {foreach $config as $key => $value}
          {if is_array($value) || $key == "license_owner"}{continue}{/if}
          <div class="input-group">
            <div class="input-group-prepend" >
              <span class="input-group-text capitialize" id="inputGroup-sizing-lg">{$key}:</span>
            </div>
            <input name="Change{$key}" type="text" class="form-control form-lg form-control-lg" placeholder="{$key}"
              value="{$value}">
            <script>
              $('[name="Change{$key}"]').on('change', function (e) {
                toastr.success('Einstellungen gespeichert');
                e.preventDefault();
                var value = $(this).val();
                setConfig('{$key}', value);
              });
            </script>
          </div>
          {/foreach}
        </div>
        <hr>
        <button type="button" id="cleanUnique" class="btn admin-bg-red white-text btn-rounded capitialize btn-block">Nicht genutzte Bilder im Uploads Ordner löschen</button>
        <script>
          $('#cleanUnique').on('click', function (e) {
            $.get('/core/includes/admin/cleanuniquefolder.inc.php', {

            }).then(response => {
              if (response != "Wirklich?") {
                toastr.success('/uploads/unique/ Ordner wurde aufgeräumt.');
              } else {
                toastr.warning('Drücke innerhalb von 10 sekunden erneut auf die Schaltfläche!', null, { timeOut: 10000 });
              }
              $(this).html(response);
            })
          });
        </script>
      </div>
    </div>
  </div>
</div>
