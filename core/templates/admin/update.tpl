<h1>{$update_text}</h1>
<hr>
<div class="row">
  <div class="col-lg-5">
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        {$update_text}
      </div>
      <div class="card-body px-4">
        <h4 class="admin-text">{$current_version}: {VERSION}</h4>
        {if version_compare($update['website_version'], VERSION, '!=')}
        <hr>
        <h5 class="red-text">{$update_website_text1}:
          {$update['website_version']}</h5>
          <h6 class="admin-text">{$update_website_text2}</h6>
          <a class="btn admin-bg-purple btn-rounded btn-sm white-text" href="https://api.zooeey.de/updates/{WEBSITE_NAME}">{$download}</a>
          <a class="btn admin-bg-green btn-rounded btn-sm white-text" href="https://api.zooeey.de/updates/{WEBSITE_NAME}">{$update_text}</a>
          {else}
          <hr>
          <h5 class="green-text">{$website_up_to_date}</h5>
          {/if}
        </div>
      </div>
    </div>
    {if version_compare($update['website_version'], VERSION, '!=')}
    <div class="col-lg">
      <div class="card mb-4">
        <div class="card-header white-text admin-card-header">
          {$changelog}
        </div>
        <div class="card-body">
          <h3>Version {$update['website_version']} - Pushed out {$update['website_last_update']}</h3>
          <hr>
          {$update['website_changelog']}
      </div>
    </div>
  </div>
  {/if}
  </div>
