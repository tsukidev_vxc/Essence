<h1 class="text-break">{$dashboard_title}</h1>
<hr>
<div class="row">
  <div class="col-lg-8">
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        {$site_info}
      </div>
      <div class="card-body">
        <p style="font-size: 20px;">{$dashboard_text}</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <!-- Changelog Card -->
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        {$changelog}
      </div>
      <div class="card-body">
        <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="false">
          {foreach from=$change_log key=k item=v}
          <div class="card mb-4">
            <div class="card-header" role="tab" id="heading">
              <a class="admin-hover" data-toggle="collapse" data-parent="#accordionEx" href="#collapse{$k}"
                aria-expanded="false" aria-controls="collapse">
                <h5 class="mb-0">
                  {$k} <i class="fas fa-angle-down rotate-icon"></i>
                </h5>
              </a>
            </div>
            <div id="collapse{$k}" class="collapse" role="tabpanel" aria-labelledby="heading"
              data-parent="#accordionEx">
              <div class="card-body">
                {$v}
              </div>
            </div>
          </div>
          {/foreach}
        </div>
      </div>
    </div>

    <!-- Online status-->
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        {$online_text}
      </div>
      <div class="card-body">
        <!-- Online API Check -->
          {if $check_api_server}
        <div class="alert white-text admin-bg-green" role="alert">
          <i class="fas fa-check"></i> <a>api.zooeey.de</a><a class="float-right">{$online}!</a>
        </div>
          {else}
        <div class="alert white-text admin-bg-red" role="alert">
          <i class="fas fa-times"></i> <a>api.zooeey.de</a><a class="float-right">{$offline}!</a>
        </div>
          {/if}
        <!-- Online Client Check -->
          {if $check_client_server}
        <div class="alert white-text admin-bg-green" role="alert">
          <i class="fas fa-check"></i> <a>client.zooeey.de</a><a class="float-right">{$online}!</a>
        </div>
          {else}
        <div class="alert white-text admin-bg-red" role="alert">
          <i class="fas fa-times"></i> <a>client.zooeey.de</a><a class="float-right">{$offline}!</a>
        </div>
          {/if}
        <!-- Online Download Check -->
          {if $check_download_server}
        <div class="alert white-text admin-bg-green" role="alert">
          <i class="fas fa-check"></i> <a>download.zooeey.de</a><a class="float-right">{$online}!</a>
        </div>
          {else}
        <div class="alert white-text admin-bg-red" role="alert">
          <i class="fas fa-times"></i> <a>download.zooeey.de</a><a class="float-right">{$offline}!</a>
        </div>
          {/if}
      </div>
    </div>
  </div>
</div>
</div>
