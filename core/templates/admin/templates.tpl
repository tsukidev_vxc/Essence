<h1>{$template}</h1>
<hr>
<div class="row">
  <div class="col-lg">
    <div class="row">
      <div class="col-lg">
        <div class="card">
          <div class="card-header white-text admin-bg-green">
            Active Templates
            <a class="float-right white-text" href="/admin/templates">Scan for new Templates</a>
          </div>
          <div class="card-body">

            <!-- Template -->
            {foreach $getTemplates as $i}
              {if !$i.Template_Active}{continue}{/if}
              <a>{$i.Template_Name} {if $i.Template_Standard}<span class="green-text font-italic">Aktiv</span>{/if}
                <button onclick="changeTemplateState('{$i.Template_ID}', 0)"
                  class="btn btn-rounded btn-sm admin-bg-red float-right white-text {if $i.Template_Standard}disabled{/if}">Deaktivieren</button>
                <button onclick="setDefault('{$i.Template_ID}')"
                  class="btn btn-rounded btn-sm admin-bg-green float-right white-text {if $i.Template_Standard}disabled{/if}">Zum
                  Standart machen</button>
              </a>
            {/foreach}
            <!-- /Template -->

          </div>
        </div>
      </div>
      <div class="col-lg">
        <div class="card">
          <div class="card-header white-text admin-bg-red">
            Hidden Templates
          </div>
          <div class="card-body">

            <!-- Template -->
            {foreach $getTemplates as $i}
              {if $i.Template_Active}{continue}{/if}
              <a>{$i.Template_Name}
                <a onclick="changeTemplateState('{$i.Template_ID}', 1)"
                  class="btn btn-rounded btn-sm admin-bg-green float-right white-text">Aktivieren</a>
              </a>
            {/foreach}
            <!-- /Template -->

          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-5">
    <div class="card mb-4">
      <div class="card-header white-text admin-card-header">
        {$languages}
      </div>
      <div class="card-body">
        {$lang_info}
        <hr>
        <div class="row">
          <div class="col-lg">
            <div class="media z-depth-1 rounded">
              <i style="height: 100%"
                class="fas fa-globe-europe fa-lg admin-bg-green z-depth-1 p-4 rounded-left text-white mr-3"></i>
              <div class="media-body p-1">
                <p class="text-uppercase mb-1"><small>{$online_languages}:</small></p>
              </div>
            </div>
            <div class="card px-4 pt-2 pb-2 lh-1 mt-3">
              {foreach from=$available_languages key=myId item=i}
              <a>{$i.flag} {$i.long}</a>
              {/foreach}
            </div>
          </div>
          <div class="col-lg">
            <div class="media z-depth-1 rounded">
              <i class="fas fa-unlink fa-lg admin-bg-red z-depth-1 p-4 rounded-left text-white mr-3"></i>
              <div class="media-body p-1">
                <p class="text-uppercase mb-1"><small>{$broken_language}:</small></p>
              </div>
            </div>
            <div class="card px-4 pt-2 pb-2 lh-1 mt-3">
              {foreach from=$broken_languages key=myId item=i}
              <a class="red-text font-bolder font-italic">{$i.short}</a>
              {/foreach}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
