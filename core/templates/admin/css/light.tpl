<style>
    .admin-bg {
        background-color: #F4F2F7;
    }

    .modal-dialog .modal-content {
        background-color: white;
        color: #4f4f4f;
    }

    .card {
        background-color: white;
    }

    a {
        color: #27242B;
    }

    .admin-text {
        color: #4f4f4f;
    }

    .list-group-item.active {
        border: none;
        background-color: #dc3545 !important;
    }

    .admin-card-header {
        background-color: #4285f4;
    }

    .admin-bg-purple {
        background-color: purple;
    }

    .admin-bg-blue {
        background-color: #007bff;
    }

    .admin-bg-red {
        background-color: #dc3545;
    }

    .admin-bg-green {
        background-color: #28a745;
    }

    .admin-bg-orange {
        background-color: orange;
    }

    .admin-hover:hover {
        color: #27242B;
    }

    .md-accordion .card .card-body {
        font-size: .9rem;
        line-height: 1.7;
        font-weight: 300;
        color: #626262;
    }

    button.disabled {
        cursor: not-allowed;
    }
</style>