<style>
    .admin-bg {
        background-color: #1f1f1f;
        color: white;
    }

    .modal-dialog .modal-content {
        background-color: #212121;
        color: white;
    }

    .card {
        background-color: #212121;
    }

    a {
        color: white;
    }

    .admin-text {
        color: white;
    }

    .list-group-item.active {
        border: none;
        background-color: #332940 !important;

    }

    .admin-card-header {
        background-color: #332940;
    }

    .admin-bg-purple {
        background-color: #264D4A;
    }

    .admin-bg-blue {
        background-color: #05459F;
    }

    .admin-bg-red {
        background-color: #332940;
    }

    .admin-bg-green {
        background-color: #3D3A41;
    }

    .admin-bg-orange {
        background-color: #AC3806;
    }

    .admin-table {
        color: white;
    }

    .page-link {
        color: white !important;
    }

    .page-link:hover {
        background-color: rgba(44, 44, 44) !important;
    }

    .admin-hover:hover {
        color: white;
    }

    .md-accordion .card .card-body {
        font-size: .9rem;
        line-height: 1.7;
        font-weight: 300;
        color: white;
    }

    .form-control::placeholder {
        color: lightgray;
    }

    .form-control,
    .form-control:focus {
        background-color: rgb(60, 60, 60);
        border: none;
        color: white;
    }

    .form-control-lg,
    .form-control-lg:focus,
    .custom-file-label {
        background-color: rgb(60, 60, 60);
        border: none;
        color: white;
    }

    .input-group-text,
    .custom-file-input,
    .custom-file-text {
        background-color: rgb(40, 40, 40);
        border: none;
        color: white;
    }

    button.disabled {
        cursor: not-allowed;
    }
</style>