<h1>Essence <a style="font-size: 1rem;">Version {VERSION}</a></h1>
<hr>
<div class="row">
    <div class="col-md-4">
        <h5>{$license_owner} {$config['license_owner']}<br>
            {$license_key}: {$config['license']}
        </h5>
        <hr>

        <h5>
            {$important_links}:<br>
            <a href="https://github.com/SGZoey/Essence"><i class="fab fa-github"></i>
                GitHub/Essence</a><br>
            <a href="https://github.com/SGZoey"><i class="fab fa-github"></i> GitHub/SGZoey</a><br>
            <a href="https://github.com/levenour"><i class="fab fa-github"></i> GitHub/leVenour</a>
        </h5>
        <hr>
        <h5>
            Contact possibilities:<br>
            <i class="fab fa-discord text-primary"></i> Zoey#1960<br>
            <i class="fab fa-discord text-primary"></i> leVenour#1997
        </h5>
        <hr>
        <h5>
            MIT License<br>
            <br>
            Copyright &copy; 2020 Zoey Kaiser
            <br>
            Permission is hereby granted, free of charge, to any person obtaining a copy<br>
            of this software and associated documentation files (the "Software"), to deal<br>
            in the Software without restriction, including without limitation the rights<br>
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br>
            copies of the Software, and to permit persons to whom the Software is<br>
            furnished to do so, subject to the following conditions:<br>
            <br>
            The above copyright notice and this permission notice shall be included in all<br>
            copies or substantial portions of the Software.<br>
            <br>
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
            IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
            FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
            AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
            LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
            OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
            SOFTWARE.
        </h5>
        <h6 class="sub-text intro-text">
            &copy; Essence 2020 - Designed and developed by leVenour & Zoey
            </h>
    </div>
</div>