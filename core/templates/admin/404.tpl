<div class="row h-90 align-items-center mt-5">
  <div class="col-12 text-center">
    <h1 class="404 intro-text">Error 404 - Not Found</h1>
    <h2 class="sub-text intro-text">{$site_not_found}</h2>
  </div>
</div>
