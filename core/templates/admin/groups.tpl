<h1 class="text-break">{$groups}</h1>
<hr>
<div class="row">
    <div class="col-lg-4">
        <div class="card mb-4">
            <div class="card-header white-text admin-card-header">
                {$stats}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg">
                        <div class="media z-depth-1 rounded">
                            <i
                                class="fas fa-layer-group fa-lg admin-bg-red z-depth-1 p-4 rounded-left text-white mr-3"></i>
                            <div class="media-body p-1">
                                <p class="text-uppercase mb-1"><small>{$total_groups}</small></p>
                                <h5 class="font-weight-bold mb-0">{count($getGroups)}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="media z-depth-1 rounded">
                            <i
                                class="fas fa-arrow-up fa-lg admin-bg-blue z-depth-1 p-4 rounded-left text-white mr-3"></i>
                            <div class="media-body p-1">
                                <p class="text-uppercase mb-1"><small>{$newest_group}</small></p>
                                <h5 class="font-weight-bold mb-0">{(count($getGroups) > 0) ? $getGroups[0].Group_Name :
                                    "No Groups"}
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header white-text admin-card-header">
                {$add_group}
            </div>
            <div class="card-body">
                <form id="Addgroup_form">
                    <h6 class="red-text font-small text-center">
                        Wichtige Meldung: <br>
                        Wenn du eine Gruppe kopieren willst, drück auf den Stift und ändere den Gruppen Namen andererseits wird die Gruppe editiert.
                    </h6>
                    <div class="input-number" name="GroupSort" min="1">{$group_sort}</div>
                    <a class="pl-1">{$group_name}:</a>
                    <div class="input-group">
                        <input type="text" class="form-control" name="GroupName" required>
                    </div>
                    <a class="pl-1">{$group_color}:</a>
                    <div class="input-group">
                        <input type="text" class="form-control" name="GroupColor" required>
                    </div>
                    <a class="pl-1">{$group_html} <span class="red-text">({$group_html_info})</span>:</a>
                    <div class="input-group">
                        <input type="text" class="form-control" name="GroupHTML1" required>
                    </div>
                    <input hidden type="text" class="form-control" name="GroupHTML2" required value="null">
                    <br>
                    <h6 class="text-center red-text"><span id="addgroup-result"></span></h6>
                    <button type="submit"
                        class="btn admin-bg-green btn-rounded btn-block white-text">{$add_group}</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card mb-4">
            <div class="card-header white-text admin-card-header">
                {$all_groups}
            </div>
            <div class="card-body">
                <table id="groups_list" class="admin-table table table-striped table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm"><i class="fas fa-sort"></i>
                            </th>
                            <th class="th-sm">{$group_html}
                            </th>
                            <th class="th-sm">{$actions}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $getGroups as $i}
                        <tr id="{$i.Group_ID}">
                            <td>{$i.Group_Sort}</td>
                            <td style="display: none;"><a
                                    style="color: {$i.Group_Color}; font-weight: bold">{$i.Group_Name}</a></td>
                            <td>{$i.Group_HTML}</td>
                            <td style="display: none;">{$i.Group_Color}</td>
                            <td style="display: none;">{$i.Group_HTML_2}</td>
                            <td>
                                <button class="btn admin-bg-blue btn-sm btn-rounded editgroup white-text"><i
                                        class="fas fa-pen"></i></button>
                                <button class="btn admin-bg-orange btn-sm btn-rounded manageperms white-text"><i
                                        class="fas fa-shield-alt"></i></button>
                                <button class="btn admin-bg-purple btn-sm btn-rounded showusers white-text"><i
                                        class="fas fa-users"></i></button>
                                <button class="btn admin-bg-red btn-sm btn-rounded deletegroup white-text" {if
                                    $i.Group_Name==Group::$DEFAULT_GROUP}disabled{/if}><i
                                        class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</div>