<h1 class="text-break">{$users}</h1>
<hr>
<div class="row">
    <div class="col-lg-4">
        <div class="card mb-4">
            <div class="card-header white-text admin-card-header">
                {$stats}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg">
                        <div class="media z-depth-1 rounded">
                            <i class="fas fa-users fa-lg admin-bg-red z-depth-1 p-4 rounded-left text-white mr-3"></i>
                            <div class="media-body p-1">
                                <p class="text-uppercase mb-1"><small>Total Users</small></p>
                                <h5 class="font-weight-bold mb-0">{count($getUsers)}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="media z-depth-1 rounded">
                            <i
                                    class="fas fa-calendar-week fa-lg admin-bg-blue z-depth-1 p-4 rounded-left text-white mr-3"></i>
                            <div class="media-body p-1">
                                <p class="text-uppercase mb-1"><small>Newest User</small></p>
                                <h5 class="font-weight-bold mb-0">{$getUsers[0].User_Name}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card mb-4">
            <div class="card-header white-text admin-card-header">
                Alle {$users}
            </div>
            <div class="card-body">
                <table id="users_list" class="admin-table table table-striped table-sm" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">UserName
                        </th>
                        <th class="th-sm">UserEmail
                        </th>
                        <th class="th-sm">UserGroup
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $getUsers as $i}
                        {$user = User::getUserByName($i.User_Name)}
                        <tr id="{$i.UserID}">
                            <td><img height="20px" width="20px"
                                     src="{if array_key_exists('avatar', $user->Info)}{$user->Info['avatar']}{else}/uploads/essence/no_profile.png{/if}">
                                {$user->UserName}</td>
                            <td class="text-break">{$user->UserMail}</td>
                            <td>
                                <select name="selectChangeGroup"
                                        class="browser-default custom-select admin-bg border-0">
                                    {foreach $getGroups as $g}
                                        <option value="{$g.Group_Name}"
                                                {if $user->Group->Name == $g.Group_Name}selected{/if}>{$g.Group_Name}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>