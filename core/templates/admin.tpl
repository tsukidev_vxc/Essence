{if $theme == "light"}
{include "admin/css/light.tpl"}
{else}
{include "admin/css/dark.tpl"}
{/if}
{include file="scripts/admin_scripts.tpl"}

<div class="d-flex" id="wrapper">
  <center>
    <div class="bg-dark" id="sidebar-wrapper">
      <div class="sidebar-heading white-text">
        <a href="/admin"><img height="75px" src="{$config['image:upload']['logo']}"></img></a>
      </div>
      <div class="list-group list-group-flush">
        <a href="/admin" class="{if $Page == "dashboard"}active{/if} list-group-item list-group-item-action bg-dark
          white-text">{$dashboard}</a>
        <a href="/admin/settings" class="{if $Page == "settings"}active{/if} list-group-item list-group-item-action
          bg-dark white-text">{$settings}</a>
        <a href="/admin/users" class="{if $Page == "users"}active{/if} list-group-item list-group-item-action bg-dark
          white-text">{$users}</a>
        <a href="/admin/groups" class="{if $Page == "groups"}active{/if} list-group-item list-group-item-action bg-dark
          white-text">{$groups}</a>
        <a href="/admin/templates" class="{if $Page == "templates"}active{/if} list-group-item list-group-item-action bg-dark
          white-text">{$template}</a>
        <a href="/admin/update" class="{if $Page == "update"}active{/if} list-group-item list-group-item-action bg-dark
          white-text">{$update_text}</a>
      </div>
    </div>
  </center>
  <div id="page-content-wrapper" class="admin-bg">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <button class="btn btn-sm white-text" id="menu-toggle">{$show_menu}</button>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <div class="dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">{$user->UserName}</a>
              <!--Menu-->
              <div class="dropdown-menu dropdown-primary">
                <a class="dropdown-item" href="/settings">{$settings}</a>
                <a class="dropdown-item" href="/logout">{$logout}</a>
              </div>
            </div>
          </li>
          <li class="nav-item"><a class="btn white-text admin-bg-blue btn-sm btn-rounded" href="/">{$main_site}</a></li>
          <li class="nav-item">
            <div class="dropdown">
              <a class="btn white-text admin-bg-red btn-sm btn-rounded" data-toggle="dropdown">{$lang}</a>
              <!--Menu-->
              <div class="dropdown-menu dropdown-primary">
                {foreach from=$available_languages key=myId item=i}
                <a href="/admin?lang={$i.short}&link={$Link}" class="dropdown-item" href="?lang=DE">{$i.flag} {$i.long}</a>
                {/foreach}
              </div>
            </div>
          </li>
          {if $theme == "light"}
          <li class="nav-item"><a href="/admin?theme=dark&link={$Link}"
              class="btn btn-purple btn-rounded btn-sm px-3 changeTheme"><i class="fas fa-moon"></i></a></li>
          {else}
          <li class="nav-item"><a href="/admin?theme=light&link={$Link}"
              class="btn btn-primary btn-rounded btn-sm px-3 changeTheme"><i class="fas fa-sun"></i></a></li>
          {/if}
          <li class="nav-item"><a href="/admin/information"
              class="btn white-text admin-bg-blue btn-rounded btn-sm px-3"><i class="fas fa-question"></i></a></li>
        </ul>
      </div>
    </nav>

    {if $config['sitesettings:switch']['maintenance']}
    <div class="admin-bg-red" style="text-align: center; color: white">
      Die Website befindet sich gerade im Wartungsmodus! Normale User werden sie nicht betrachten können! <a
        class="btn white-text admin-bg-purple btn-rounded btn-sm" href="/admin/settings">Zu den Einstellungen</a>
    </div>
    {/if}

    <div class="container-fluid mt-4 animated fadeIn">
      {if $Page == "dashboard"}

      {include "admin/dashboard.tpl"}

      {elseif $Page == "groups"}

      {include "admin/groups.tpl"}

      {elseif $Page == "settings"}

      {include "admin/settings.tpl"}

      {elseif $Page == "information"}

      {include "admin/information.tpl"}

      {elseif $Page == "update"}

      {include "admin/update.tpl"}

      {elseif $Page == "users"}

      {include "admin/users.tpl"}

      {elseif $Page == "templates"}

      {include "admin/templates.tpl"}

      {else}

      {include "admin/404.tpl"}

      {/if}
    </div>
  </div>
</div>
{include "admin/Modals.tpl"}
