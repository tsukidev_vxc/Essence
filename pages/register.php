<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Register";
$link = "/register";
require $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';

if ($user->Login == "true") {
  header('location: /');
}

if (!$config->get('sitesettings:switch')['userregistration']) {
  $smarty->assign('route', $_SERVER['REQUEST_URI']);
  $smarty->assign('error_code', '404');
  require($_SERVER['DOCUMENT_ROOT'] .'/core/includes/template/generate.php');
  $smarty->display('404.tpl');
  die();
}

if (isset($_POST['UserName']) && isset($_POST['UserMail']) && isset($_POST['Password1']) && isset($_POST['Password2'])) {
    header('Content-Type: application/json');
    $TempUserName = $_POST['UserName'];
    $TempUserMail = $_POST['UserMail'];
    $TempPassword1 = $_POST['Password1'];
    $TempPassword2 = $_POST['Password2'];
    $PasswordHash = md5($TempPassword1);

    if ($TempPassword1 != $TempPassword2) {
        die(json_encode(array(
            'status' => 'false',
            'message' => $language['passwords_not_match']
        )));
    }

    $stmt = $db->prepare("SELECT * FROM ec_users WHERE User_Name = ? OR User_Email = ?");
    $stmt->bind_param('ss', $TempUserName, $TempUserMail);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows <= 0) {
        $stmt = $db->prepare("INSERT INTO ec_users (User_Name, User_Password, User_Email) VALUES (?,?,?)");
        $stmt->bind_param('sss', $TempUserName, $PasswordHash, $TempUserMail);
        $stmt->execute();
        die(json_encode(array(
            'status' => 'true',
            'message' => $language['success_register']
        )));
    } else {
        die(json_encode(array(
            'status' => 'false',
            'message' => $language['username_or_email_already_exists']
        )));
    }
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php' ?>
</head>

<body>
    <?php
    $smarty->display('register.tpl');
    ?>
    <script>
        $('#Register_form').submit(function (e) {
            e.preventDefault();
            var username = $('[name=UserName]').val();
            var usermail = $('[name=UserMail]').val();
            var password1 = $('[name=Password1]').val();
            var password2 = $('[name=Password2]').val();
            $.post('/register/', {
                UserName: username,
                UserMail: usermail,
                Password1: password1,
                Password2: password2
            }).then(function (response) {
                var json = JSON.parse(JSON.stringify(response));
                $('#RegisterResult').html(json.message);
                $('#RegisterResult').css("color", json.status == "true" ? 'green' : 'red');
                if(json.status == "true"){
                  window.location.replace("/login");
                }
            });
        });
    </script>

</body>

</html>
