<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Home";
$link = "/";
require $_SERVER['DOCUMENT_ROOT'] .'/core/init.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php' ?>
</head>

<body>
  <?php
  $smarty->display('index.tpl');
  ?>

</body>

</html>
