<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Profile";
$link = "/profile";
require $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';

if (isset($_GET['username']) && User::getUserByName($_GET['username']) != null) {
    $smarty->assign('from', User::getUserByName($_GET['username']));
} else {
    header('location: /');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php' ?>
</head>

<body>
<?php
$smarty->display('profile.tpl');
?>
</body>

</html>