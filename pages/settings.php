<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Settings";
$link = "/settings";
require $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';

if ($user->Login == "false") {
    die('<meta http-equiv="refresh" content="0;url=/">');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php' ?>
</head>

<body>
    <?php
    $smarty->display('settings.tpl');
    ?>

</body>

</html>