<?php
/*
_____
|  ___|
| |__ ___ ___  ___ _ __   ___ ___
|  __/ __/ __|/ _ \ '_ \ / __/ _ \
| |__\__ \__ \  __/ | | | (_|  __/
\____/___/___/\___|_| |_|\___\___|

♡ Code by leVenour and Zoey ♡

➤ leVenour.at
➤ Zooeey.de
*/
$page = "Login";
$link = "/login";
require $_SERVER['DOCUMENT_ROOT'] .'/core/init.php';

if ($user->Login == "true") {
  header('location: /');
}

if (isset($_POST['UserName']) && isset($_POST['UserPassword']) && isset($_POST['StayLogin'])) {
  header('Content-Type: application/json');
  $TempUserName = $_POST['UserName'];
  $TempPassword = $_POST['UserPassword'];
  $PasswordHash = md5($TempPassword);
  $StayLogin = $_POST['StayLogin'];

  $stmt = $db->prepare("SELECT * FROM ec_users WHERE User_Name = ? AND User_Password = ?");
  $stmt->bind_param('ss', $TempUserName, $PasswordHash);
  $stmt->execute();
  $result = $stmt->get_result();
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()){
      if($StayLogin){
        setcookie('User_Name', $row['User_Name'], time()+60*60*24*30, '/');
        setcookie('User_Password', $row['User_Password'], time()+60*60*24*30, '/');
      } else {
        $_SESSION['User_Name'] = $row['User_Name'];
        $_SESSION['User_Password'] = $row['User_Password'];
      }
    }
    die(json_encode(array(
      'status' => 'true',
      'message' => $language['success_login']
    )));
  } else {
    die(json_encode(array(
      'status' => 'false',
      'message' => $language['invalid_username_or_password']
    )));
  }
  die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php' ?>
</head>

<body>
  <?php
  $smarty->display('login.tpl');
  ?>
  <script>
  $('#Login_form').submit(function(e) {
    e.preventDefault();
    var username = $('[name=UserName]').val();
    var password = $('[name=Password]').val();
    var staylogin = $('[name=StayLogin]').is(':checked');
    $.post('/login/', {
      UserName: username,
      UserPassword: password,
      StayLogin: staylogin
    }).then(function(response) {
      var json = JSON.parse(JSON.stringify(response));
      $('#LoginResult').html(json.message);
      $('#LoginResult').css("color", json.status == "true" ? 'green' : 'red');
      if(json.status == "true"){
        location.reload();
      }
    });
  });
  </script>
</body>

</html>
