<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Install";
$link = "/install";
require $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';

// Assign Infos
$smarty->assign('Title', 'Install');
$smarty->assign('Date', date('Y'));
$smarty->assign('step', (isset($_COOKIE['install_step']) ? $_COOKIE['install_step'] : ""));
$smarty->assign('key', ($config->get('license') != null ? $config->get('license') : ""));
$smarty->assign('key_for', ($config->get('license_owner') != null ? $config->get('license_owner') : ""));

// Assign Checks
$smarty->assign('LicenseServerOnline', WebsiteOnlineCheck::checkOnline('api.zooeey.de'));
$smarty->assign('PHPVersion', phpversion());
$smarty->assign('CompatiblePHPVersion', version_compare(phpversion(), '5.4', '>='));
if (is_writable($_SERVER['DOCUMENT_ROOT'] . '/core')) {
    $smarty->assign('ConfigWritable', true);
} else {
    $smarty->assign('ConfigWritable', false);
}
if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/cache/templates')) {
    try {
        mkdir($_SERVER['DOCUMENT_ROOT'] . '/cache/templates', 0777, true);
        $smarty->assign('CreatedCacheFile', true);
    } catch (Exception $e) {
        $smarty->assign('CreatedCacheFile', false);
    }
} else {
    $smarty->assign('CreatedCacheFile', true);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require($_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php');
    ?>
    <style>
        html,
        body {
            background-image: url('/uploads/essence/background.jpg');
        }
    </style>
</head>

<body>
    <?php
    $smarty->display('install.tpl');
    ?>
</body>

</html>