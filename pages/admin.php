<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Admin";
if (isset($_GET['page'])) {
    $link = "/admin/" . $_GET['page'];
} else {
    $link = "/admin";
}

if (isset($_GET['theme'])) {
    $direct_to = $_GET['link'];
    setcookie('admin_theme', $_GET['theme'], time() + (86400 * 365), "/");
    header('location: ' . $direct_to);
}

require $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';

if ($user->Login == "false") {
    header('location: /login');
}

if ($link == "/admin/update") {
    $updateChecker = new UpdateChecker(WEBSITE_NAME);
    if (count($updateChecker->data) <= 0) $updateChecker->checkUpdate();
    $smarty->assign('update', $updateChecker->data);
}

include($_SERVER['DOCUMENT_ROOT'] . '/core/changelog.php');
include($_SERVER['DOCUMENT_ROOT'] . '/core/includes/string_search.inc.php');
$smarty->assign('Link', $link);
$smarty->assign('Page', (isset($_GET['page']) ? $_GET['page'] : "dashboard"));
$smarty->assign('theme', (isset($_COOKIE['admin_theme']) ? $_COOKIE['admin_theme'] : "light"));

/*Connections to the zooeey.de servers */
//$smarty->assign('license', $licenseChecker->data);
//$smarty->assign('update', $updateChecker->data);
$smarty->assign('check_api_server', WebsiteOnlineCheck::checkOnline('api.zooeey.de'));
$smarty->assign('check_client_server', WebsiteOnlineCheck::checkOnline('client.zooeey.de'));
$smarty->assign('check_download_server', WebsiteOnlineCheck::checkOnline('download.zooeey.de'));
$smarty->assign('broken_languages', $lang->broken_languages());

// Get Database
$groups = array();
$getGroups = $db->query('SELECT * FROM ec_groups ORDER BY Group_Sort ASC');
while ($row = $getGroups->fetch_assoc())
    $groups[] = $row;
$smarty->assign('getGroups', $groups);
$templates = array();
$getTemplates = $db->query('SELECT * FROM ec_templates ORDER BY Template_ID DESC');
while ($row = $getTemplates->fetch_assoc())
    $templates[] = $row;
$smarty->assign('getTemplates', $templates);
$users = array();
$getUsers = $db->query('SELECT * FROM ec_users ORDER BY UserID DESC');
while ($row = $getUsers->fetch_assoc())
    $users[] = $row;
$smarty->assign('getUsers', $users);

$smarty->assign('change_log', $changelog);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require($_SERVER['DOCUMENT_ROOT'] . '/core/includes/template/generate.php');
    ?>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Muli&display=swap');

        body,
        html {
            font-family: 'Muli';
            overflow-x: hidden;
        }

        .sidebar-heading {
            position: sticky;
        }

        #sidebar-wrapper {
            min-height: 100vh;
            height: 100%;
            margin-left: -15rem;
            -webkit-transition: margin .25s ease-out;
            -moz-transition: margin .25s ease-out;
            -o-transition: margin .25s ease-out;
            transition: margin .25s ease-out;
        }

        #sidebar-wrapper a.active {
            background-color: red;
        }

        #sidebar-wrapper .sidebar-heading {
            padding: 0.875rem 1.25rem;
            font-size: 1.2rem;
        }

        #sidebar-wrapper .list-group {
            width: 15rem;
        }

        #page-content-wrapper {
            min-width: 100vw;
        }

        #wrapper.toggled #sidebar-wrapper {
            margin-left: 0;
        }

        @media (min-width: 768px) {
            #sidebar-wrapper {
                margin-left: 0;
            }

            #page-content-wrapper {
                min-width: 0;
                width: 100%;
            }

            #wrapper.toggled #sidebar-wrapper {
                margin-left: -15rem;
            }
        }

        .list-group-item {
            border: none;
        }
    </style>
</head>

<body>
    <?php
    $smarty->display('admin.tpl');
    ?>

</body>

</html>