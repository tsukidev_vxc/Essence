<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/
$page = "Error";
$link = "/";
require $_SERVER['DOCUMENT_ROOT'] .'/core/init.php';
$smarty->assign('route', $_SERVER['REQUEST_URI']);
$smarty->assign('error_code', $_GET['e']);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require($_SERVER['DOCUMENT_ROOT'] .'/core/includes/template/generate.php');
  ?>
</head>

<body>
  <?php
  $smarty->display('404.tpl');
  ?>

</body>

</html>
