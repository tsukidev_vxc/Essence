{include "navbar.tpl"}
<div class="container mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">{$config['siteinformation:input']['title']}</li>
            <li class="breadcrumb-item active">Profile</li>
            <li class="breadcrumb-item active">{$from->UserName}</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-lg-4 pb-5">
            <div class="card card-body">
                <div class="author-card pb-3">
                    <div class="author-card-cover" style="background-image: url('/uploads/essence/background.jpg')">
                    </div>
                    <div class="author-card-profile">
                        <div class="author-card-avatar card-img-100">
                            <img src="{$from->getAvatar()}">
                        </div>
                        <div class="author-card-details pl-2">
                            <h2 class="author-card-name white-text">{$from->UserName} {$from->Group->HTML_1}</h2>
                        </div>
                    </div>
                    <h6 class="text-center mx-3">{$from->getInfo('status')}</h6>
                    <nav id="v-pills-tab" role="tablist" class="list-group list-group-flush mt-3">
                        <a class="list-group-item active" id="v-pills-home-tab" data-toggle="pill"
                           href="#v-pills-bulletin"
                           role="tab" aria-controls="v-pills-home" aria-selected="true">Pinnwand</a>
                        <a class="list-group-item" id="v-pills-social-tab" data-toggle="pill" href="#v-pills-social"
                           role="tab" aria-controls="v-pills-social" aria-selected="false">Social Media</a>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Profile View -->
        <div class="col-lg-8 pb-5">
            <div class="card card-body text-center">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-bulletin" role="tabpanel"
                         aria-labelledby="v-pills-home-tab">
                        <!-- TODO: Some information here -->
                    </div>
                    <div class="tab-pane fade" id="v-pills-social" role="tabpanel"
                         aria-labelledby="v-pills-profile-social">
                        <div class="row justify-content-center">
                            <div class="col-md-2">
                                <a class="red-text" href="{$from->getInfo('youtube')}"><i
                                            class="fab fa-youtube fa-5x"></i></a>
                                Youtube
                            </div>
                            <div class="col-md-2">
                                <a class="light-blue-text" href="{$from->getInfo('twitter')}"><i
                                            class="fab fa-twitter fa-5x"></i></a>
                                Twitter
                            </div>
                            <div class="col-md-2">
                                <a class="black-text" href="{$from->getInfo('github')}"><i
                                            class="fab fa-github fa-5x"></i></a>
                                GitHub
                            </div>
                            <div class="col-md-2">
                                <a class="amber-text" href="{$from->getInfo('namemc')}"><i
                                            class="fas fa-hourglass fa-5x"></i></a>
                                NameMC
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
{include "footer.tpl"}