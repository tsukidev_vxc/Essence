</div> <!-- Important DO NOT REMOVE -->

<!-- Edit From Here -->
<div class="bg-primary mt-5" style="padding-top: 5px; padding-bottom: 5px;">
  <div class="container">
    <button type="button" class="btn btn-primary btn-rounded btn-sm capitalize">
      Templates
    </button>
    <button type="button" class="btn btn-primary btn-rounded btn-sm capitalize" data-toggle="modal" data-target="#sprachenpopup">
      Sprachen
    </button>
  </div>
</div>
<div class="modal fade" id="sprachenpopup" tabindex="-1" role="dialog" aria-labelledby="sprachenpopup"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sprachen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {foreach from=$available_languages key=myId item=i}
        <a href="?lang={$i.short}" class="dropdown-item" href="?lang=DE">{$i.flag} {$i.long}</a>
        {/foreach}
      </div>
    </div>
  </div>
</div>
<section class="px-md-5 py-5 text-center white-text elegant-color z-depth-1">
  <h4>{$config['siteinformation:input']['title']}</h4>
  <h6 class="">Made with <i class="fas fa-heart red-text mx-1"></i> by <a class="white-text" href="https://github.com/leVenour">leVenour</a> and <a class="white-text" href="https://github.com/SGZoey">Zoey</a></h6>
</section>
