{include file="navbar.tpl"}
<div class="container mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">{$config['siteinformation:input']['title']}</li>
        </ol>
    </nav>
    <div class="card card-body text-center">
        {if $user->Login == "true"}
            <h4>Willkommen {$user->UserName}#{$user->UserID} ({$user->UserMail}), Vielen Dank fürs anmelden ;)</h4>
            <hr>
            <div class="row">
                <div class="col-md">
                    <a href="/admin" class="btn btn-primary btn-block capatilize">Administration</a>
                </div>
                <div class="col-md">
                    <a href="/logout" class="btn btn-danger btn-block capatilize">Ausloggen</a>
                </div>
            </div>
        {else}
            <h2>Willkommen! Willst du dich anmelden?</h2>
            <hr>
            <a href="/login" class="btn btn-success btn-round capatilize">Anmelden</a>
        {/if}
    </div>
</div>
{include file="footer.tpl"}