<!-- Important DO NOT REMOVE || min-height: calc(HEIGHT OF SCREENvh - HEIGHT OF FOOTERpx) -->
<div id="wrapper" style="min-height: calc(100vh - 261px);">

    <style>
        .top-user-collapse {
            background-color: #4285f4 !important;
        }

        .userbar:not(.top-nav-collapse) {
            background: transparent !important;
        }

        @media (max-width: 768px) {
            .userbar:not(.top-nav-collapse) {
                background: transparent !important;
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {
            .userbar:not(.top-nav-collapse) {
                background: transparent !important;
            }
        }
    </style>

    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar top-user-collapse userbar">
        <div class="container">
            <a class="navbar-brand" href="/">{$config['siteinformation:input']['title']}</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- User Info Area -->
                <ul class="navbar-nav ml-auto nav-flex-icons">
                    <!-- User IS logged in -->
                    {if $user->Login == "true"}
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {$user->UserName}
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg-right dropdown-icon-navbar"
                            aria-labelledby="navbarDropdownMenuLink-55" style="border-bottom: 5px solid red">
                            <div class="row">
                                <div class="col-4 p-3">
                                    <div style="width: 80px; height: 80px; overflow: hidden;"><img
                                            style="border-radius: 4px;" class="cover-image img-fluid"
                                            src="{$user->getAvatar()}"></img></div>
                                </div>
                                <div class="col user-info-navbar pt-3" style="line-height: 2px">
                                     <h4 style="color: {$user->Group->Color}">{$user->UserName} {$user->Group->HTML_1}</h4>
                                   </div>
                            </div>
                            <hr>
                            <a class="dropdown-item" href="/settings">Einstellungen</a>
                            <a class="dropdown-item" href="/admin">Web Interface</a>
                            <a class="dropdown-item" href="/logout">Abmelden</a>
                        </div>
                    </li>
                    {else}
                    <!-- User IS NOT Logged in -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Anmelden
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/login">Anmelden</a>
                            {if $config['sitesettings:switch']['userregistration']}
                            <a class="dropdown-item" href="/register">Registieren</a>
                            {/if}
                        </div>
                    </li>
                    {/if}
                </ul>
                <!-- END USER AREA -->
            </div>
        </div>
    </nav>

    <header class="header">
        <div class="container">
            <center>
                <div class="zoom-logo">
                    <a href="/"><img style="z-index: 1; padding: 40px;" class="img-fluid" width="250px"
                            src="{$config['image:upload']['logo']}"></a><br>
                </div>
            </center>
        </div>
    </header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Nav Links -->
                <ul class="navbar-nav mr-auto">
                    {foreach $config['navbaritems'] as $k => $v}
                    <li class="nav-item">
                        <a class="nav-link" href="{$v}">{$k}</a>
                    </li>
                    {/foreach}
                </ul>
                <!-- End Nav Links -->

            </div>
    </nav>
