{include file="navbar.tpl"}
<div class="container mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">{$config['siteinformation:input']['title']}</li>
            <li class="breadcrumb-item active">{$error_code}</li>
        </ol>
    </nav>
    <div class="card card-body pb-4">
        <div class="text-center">
            <img height="150px" width="150px" src="/uploads/essence/404_cat.png">
            <h1 class="404 intro-text">Oh No! Error {$error_code}</h1>
            <h5>
                An issue came up while trying to open<br>
                <i>{$route}</i>
            </h5>
            <a class="btn btn-primary btn-round capitialize white-text" href="/">Home</a>
            <a class="btn btn-red btn-round capitialize white-text"
               onclick="window.history.go(-1); return false;">Back</a>
        </div>
    </div>
</div>
{include file="footer.tpl"}
