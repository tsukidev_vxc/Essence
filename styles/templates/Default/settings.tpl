{include "navbar.tpl"}
<div class="container mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">{$config['siteinformation:input']['title']}</li>
            <li class="breadcrumb-item">Settings</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-lg-4 pb-5">
            <div class="card card-body">
                <div class="author-card pb-3">
                    <div class="author-card-cover" style="background-image: url(/uploads/essence/background.jpg);">
                    </div>
                    <div class="author-card-profile">
                        <form id="changeAvatar_form" method="POST" enctype="multipart/form-data">
                            <input accept="image/*" id="fileToUpload" name="fileToUpload" onchange="changeAvatar()"
                                   hidden="" type="file" class="inputfile inputfile-4"
                                   data-multiple-caption="files selected">
                            <label for="fileToUpload">
                                <div class="author-card-avatar">
                                    <img src="{$user->getAvatar()}">
                                </div>
                            </label>
                        </form>
                        <div class="author-card-details">
                            <h2 class="author-card-name white-text">{$user->UserName} {$user->Group->HTML_1}</h2>
                        </div>
                    </div>
                    <a style="color: gray; font-size: 10px; font-weight: 500; padding-left: 2px;"> Klicke auf dieses
                        Bild um dein Profilbild zu ändern.</a>
                    <nav id="v-pills-tab" role="tablist" class="list-group list-group-flush mt-3">
                        <a class="list-group-item active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home"
                           role="tab" aria-controls="v-pills-home" aria-selected="true">Profile Settings</a>
                        <a class="list-group-item" id="v-pills-social-tab" data-toggle="pill" href="#v-pills-social"
                           role="tab" aria-controls="v-pills-social" aria-selected="false">Social Media</a>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Profile Settings-->
        <div class="col-lg-8 pb-5">
            <div class="card card-body">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                         aria-labelledby="v-pills-home-tab">
                        <div class="input-group">
                            <div class="input-group-text"><span class="input-group-prepend">Status</span></div>
                            <input class="form-control" type="text" id="Settings_Status"
                                   onchange="setSocial('status', ''+$(this).val(), false)"
                                   value="{$user->getInfo('status')}">
                        </div>
                        <hr>
                        <form id="changeSettings_form" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Settings_UserName">Username</label>
                                        <input class="form-control" type="text" id="Settings_UserName"
                                               name="Settings_UserName" value="{$user->UserName}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Settings_UserMail">Email Adresse</label>
                                        <input class="form-control" type="email" id="Settings_UserMail"
                                               name="Settings_UserMail" value="{$user->UserMail}" disabled>
                                    </div>
                                </div>
                            </div>
                            <hr class="mt-2 mb-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Settings_Password1">New Password</label>
                                        <input class="form-control" type="password" id="Settings_Password1"
                                               name="Settings_Password1" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Settings_Password2">Confirm Password</label>
                                        <input class="form-control" type="password" id="Settings_Password2"
                                               name="Settings_Password2" required>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <button type="submit" class="btn btn-primary btn-round capitialize text-right">
                                        Passwort ändern
                                    </button>
                                </div>
                            </div>
                            <h6 id="changeSettings_result" class="text-center red-text"></h6>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="v-pills-social" role="tabpanel"
                         aria-labelledby="v-pills-social-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-fn"><i class="fab fa-twitter"></i> Twitter</label>
                                    <input class="form-control" type="text" id="account-fn"
                                           onchange="setSocial('twitter', ''+$(this).val())"
                                           value="{$user->getInfo('twitter')}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-ln"><i class="fab fa-youtube"></i> Youtube</label>
                                    <input class="form-control" type="text" id="account-ln"
                                           onchange="setSocial('youtube', ''+$(this).val())"
                                           value="{$user->getInfo('youtube')}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-ln"><i class="fab fa-github"></i> Github</label>
                                    <input class="form-control" type="text" id="account-ln"
                                           onchange="setSocial('github', ''+$(this).val())"
                                           value="{$user->getInfo('github')}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="account-ln"><i class="fas fa-hourglass"></i> NameMC</label>
                                    <input class="form-control" type="text" id="account-ln"
                                           onchange="setSocial('namemc', ''+$(this).val())"
                                           value="{$user->getInfo('namemc')}">
                                </div>
                            </div>
                        </div>
                        <h6 class="text-center"><span id="setSocial_result"></span></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include "footer.tpl"}
<script>
    $('#changeSettings_form').submit(function (e) {
        e.preventDefault();
        var UserName = $('[name=Settings_UserName').val();
        var UserMail = $('[name=Settings_UserMail').val();
        var Password1 = $('[name=Settings_Password1').val();
        var Password2 = $('[name=Settings_Password2').val();
        if (Password1 != Password2) {
            $('#changeSettings_result').html('Die Passwörter stimmen nicht überein!');
            return;
        }
        $.post('/core/includes/user/changesettings.inc.php', {
            Password1: Password1,
            Password2: Password2
        }).then(function (response) {
            if (response == "false") {
                return;
            }
            location.reload();
        });
    });

    function setSocial(name, value, isLink = true) {
        if (!(value.startsWith('http://') || value.startsWith('https://') || value.length <= 0) && isLink) {
            $('#setSocial_result').html('Der eingegebene Text ist kein Link!');
            $('#setSocial_result').css('color', 'red');
            return;
        }
        $.post('/core/includes/user/setinfo.inc.php', {
            Key: name,
            Value: value
        }).then(function (response) {
            $('#setSocial_result').html('Social-Media saved!');
            $('#setSocial_result').css('color', 'green');
        });
    }

    function changeAvatar() {
        const files = document.querySelector('[name=fileToUpload]').files;
        const formData = new FormData();
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            formData.append('files[]', file);
        }
        formData.append('SaveAs', "avatar_{$user->UserName}");
        formData.append('Ext', "png,jpg,bmp,gif");
        fetch('/core/includes/admin/uploadfile.inc.php', {
            method: 'POST',
            body: formData
        }).then((response) => {
            return response.json();
        }).then((jsonResponse) => {
            var json = JSON.parse(JSON.stringify(jsonResponse));
            $.post('/core/includes/user/changeavatar.inc.php', {
                UserID: "{$user->UserID}",
                FileName: json.message
            }).then(function () {
                location.reload();
            })
        });
    }
</script>