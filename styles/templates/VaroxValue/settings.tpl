{include "navbar.tpl"}
<div class="container mt-3">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">{$config['siteinformation:input']['title']}</li>
      <li class="breadcrumb-item">Settings</li>
    </ol>
  </nav>
  <div class="row">
    <div class="col-lg-4 pb-5">
      <div class="card card-body">
        <div class="author-card pb-3">
          <div class="author-card-cover" style="background-image: url(/uploads/essence/background.jpg);"></div>
          <div class="author-card-profile">
            <form method="POST" enctype="multipart/form-data">
              <input accept="image/*" id="fileToUpload" name="fileToUpload" onchange="form.submit()" hidden="" type="file" class="inputfile inputfile-4" data-multiple-caption="files selected" multiple="">
              <label for="fileToUpload">
                <div class="author-card-avatar">
                  <img src="{$config['image:upload']['logo']}">
                </div>
              </label>
            </form>
            <div class="author-card-details">
              <h2 class="author-card-name white-text">{$user->UserName} <a class="lh-1 btn btn-sm btn-danger text-white btn-rounded capitialize">Admin</a></h2>
            </div>
          </div>
          <a style="color: gray; font-size: 10px; font-weight: 500; padding-left: 2px;"> Klicke auf dieses Bild um dein Profilbild zu ändern.</a>
          <nav id="v-pills-tab" role="tablist" class="list-group list-group-flush mt-3">
            <a class="list-group-item active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
            aria-controls="v-pills-home" aria-selected="true">Profile Settings</a>
            <a class="list-group-item" id="v-pills-social-tab" data-toggle="pill" href="#v-pills-social" role="tab"
            aria-controls="v-pills-social" aria-selected="false">Social Media</a>
          </nav>
        </div>
      </div>
    </div>
    <!-- Profile Settings-->
    <div class="col-lg-8 pb-5">
      <div class="card card-body text-center">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-fn">Username</label>
                  <input class="form-control" type="text" id="account-fn" value="{$user->UserName}" required="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-ln">Email Adresse</label>
                  <input class="form-control" type="email" id="account-ln" value="{$user->UserMail}" required="">
                </div>
              </div>
            </div>
            <hr class="mt-2 mb-3">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-pass">New Password</label>
                  <input class="form-control" type="password" id="account-pass">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-confirm-pass">Confirm Password</label>
                  <input class="form-control" type="password" id="account-confirm-pass">
                </div>
              </div>
            </div>
            <a class="btn btn-primary btn-round capitialize text-right">Passwort ändern</a>
          </div>
          <div class="tab-pane fade" id="v-pills-social" role="tabpanel" aria-labelledby="v-pills-profile-social">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-fn">Twitter</label>
                  <input class="form-control" type="text" id="account-fn" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-ln">Youtube</label>
                  <input class="form-control" type="text" id="account-ln" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-ln">Github</label>
                  <input class="form-control" type="text" id="account-ln" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-ln">NameMC</label>
                  <input class="form-control" type="text" id="account-ln" value="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{include "footer.tpl"}
