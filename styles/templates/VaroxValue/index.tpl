
    <div class="fixed-action-btn smooth-scroll" style="bottom: 45px; right: 24px;">
      <a href="#top-section" class="btn-floating btn-large blue">
        <i class="fas fa-arrow-up"></i>
      </a>
    </div>

    <nav class="navbar navbar-dark">
      <div class="container">
          <a class="float-left" style="color: white;" href="https://varoxcraft.de/"><i class="fas fa-arrow-left"></i> Zur Hauptseite</a>
          <a class="float-right" style="color: white;" href="https://varoxcraft.de/help/impressum/"><i class="fas fa-info"></i> Impressum</a>
      </div>
    </nav>
    <div class="container">
      <div class="varox-header animated fadeIn slow" id="top-section">
        <div class="row">
          <div class="col-lg-3 wow slideInLeft slow animated slideInLeft">
            <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
              <br><br><center><a href="index.html"><img height="175px" src="{$config['image:upload']['logo']}"></img></a>
            </div>
          </div>
          <div class="col animated slideInRight">
            <br></br>
            <select class="browser-default custom-select form-control-lg">
              <option disabled selected>Server auswählen..</option>
              <option value="1">CityBuild 1</option>
              <option value="2">CityBuild 2</option>
            </select>
            <input style="margin-top: 10px;" class="form-control form-control-lg" type="text" placeholder="Suchen..."><br>
            <div class="varox-categories">
              <a class="btn btn-pink btn-rounded varox-btn" href="cat.html"><i class="fas fa-tools"></i> Admin Items</a>
              <a class="btn btn-amber btn-rounded varox-btn" href="cat.html"><i class="fas fa-cog"></i> Redstone</a>
              <a class="btn btn-secondary btn-rounded varox-btn" href="cat.html"><i class="fas fa-building"></i> Build</a>
              <a class="btn btn-deep-orange btn-rounded varox-btn" href="cat.html"><i class="fas fa-smile"></i> Heads</a>
              <a class="btn btn-purple btn-rounded varox-btn" href="cat.html"><i class="fas fa-apple-alt"></i> Essen</a>
              <a class="btn btn-primary btn-rounded varox-btn" href="cat.html"><i class="fas fa-shield-alt"></i> Weapons</a>
              <a class="btn btn-danger btn-rounded varox-btn" href="cat.html"><i class="fas fa-hammer"></i> Tools</a>
            </div>
          </div>
        </div>
      </div>
      <div class="varox-content animated fadeIn">
        <h2 style="text-align: right" class="animated fadeIn delay-2s">Zurzeit beliebt:</h2>
        <div class="row animated slideInUp slow">
          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>

          </div>
          <hr class="animated fadeIn slowest varox-hr delay-2s">



          <div class="row animated slideInUp slow">
          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>


          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>


          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>


          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>



          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>


          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>


          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>


          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                    <center><img height="150px" src="https://crafatar.com/renders/head/069a79f444e94726a5befca90e38aaf5?scale=10"></img></center>
                  <hr>
                  <h3>Notch Head</h3>
                  <h5>122220$</h5>
                </div>
              </div>
            </div>
          </a>



          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b3/Wooden_Pickaxe.png"></img></center>
                  <hr>
                  <h3>Wooden Pickaxe</h3>
                  <h5>20$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/b/b5/Diamond_Ore_JE3_BE3.png"></img></center>
                  <hr>
                  <h3>Diamond Ore</h3>
                  <h5>140$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                  <div class="card-body">
                  <center><img height="150px" src="https://gamepedia.cursecdn.com/minecraft_gamepedia/a/a4/Water_Bottle.png"></img></center>
                  <hr>
                  <h3>Water Bottle</h3>
                  <h5>13$</h5>
                </div>
              </div>
            </div>
          </a>

          <a href="item.html" class="col-lg-3 wow slideInUp slow">
            <div class="card varox-card">
              <div class="view overlay zoom" style="cursor: pointer; user-select: none;">
                <div class="card-body">
                  <center><img height="150px" src="https://vignette.wikia.nocookie.net/minecraft/images/b/ba/Bag_Golden_Apple.png/revision/latest/top-crop/width/220/height/220?cb=20190908183623"></img></center>
                  <hr>
                  <h3>Golden Apple</h3>
                  <h5>1220$</h5>
                </div>
              </div>
            </div>
          </a>
        </div>
        <br><br>
        <br>
      </div>
    </div>
{include file="footer.tpl"}
