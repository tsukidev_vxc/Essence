<!-- Important DO NOT REMOVE || min-height: calc(HEIGHT OF SCREENvh - HEIGHT OF FOOTERpx) -->
<div id="wrapper" style="min-height: calc(100vh - 276px);">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <!-- Nav Links -->
      <ul class="navbar-nav mr-auto">
        {foreach $config['navbaritems'] as $k => $v}
        <li class="nav-item">
          <a class="nav-link" href="{$v}">{$k}</a>
        </li>
        {/foreach}
      </ul>
      <ul class="navbar-nav ml-auto nav-flex-icons">
        <!-- User IS logged in -->
        {if $user->Login == "true"}
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {$user->UserName}
          </a>
          <div class="dropdown-menu dropdown-menu-lg-right dropdown-icon-navbar"
          aria-labelledby="navbarDropdownMenuLink-55" style="border-bottom: 5px solid red">
          <div class="row">
            <div class="col-4">
              <div style="width: 80px; height: 80px; overflow: hidden;"><img style="border-radius: 4px;" class="cover-image img-fluid" src="{$config['image:upload']['logo']}"></img></div>
            </div>
            <div class="col user-info-navbar">
              <h5>{$user->UserName}</h5>
              <h7>GROUP_NAME</h7>
            </div>
          </div>
          <hr>
          <a class="dropdown-item" href="/settings">Einstellungen</a>
          <a class="dropdown-item" href="/admin">Web Interface</a>
          <a class="dropdown-item" href="/logout">Abmelden</a>
        </div>
      </li>
      {else}
      <!-- User IS NOT Logged in -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Anmelden
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/login">Anmelden</a>
          {if $config['sitesettings:switch']['userregistration']}
          <a class="dropdown-item" href="/register">Registieren</a>
          {/if}
        </div>
      </li>
      {/if}
    </ul>
      <!-- End Nav Links -->

    </div>
  </nav>
