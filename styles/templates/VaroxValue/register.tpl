{include file="navbar.tpl"}
<div class="container mt-3">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">{$config['siteinformation:input']['title']}</li>
      <li class="breadcrumb-item active">Login</li>
    </ol>
  </nav>
  <h6 class="text-center bold-text"><span id="RegisterResult"></span></h6>

  <div class="card card-signin my-3">
    <div class="card-body">
      <form id="Register_form" class="text-left" method="POST">
        <div class="input-group">
          <input type="text" name="UserName" class="form-control" placeholder="{$username}" required>
        </div>
        <div class="input-group mt-4">
          <input type="email" name="UserMail" class="form-control" placeholder="{$email}" required>
        </div>
        <div class="row mt-4">
          <div class="col-md">
            <div class="input-group">
              <input type="password" name="Password1" class="form-control" placeholder="{$password}" required>
            </div>
          </div>
          <div class="col-md">
            <div class="input-group">
              <input type="password" name="Password2" class="form-control" placeholder="{$password_repeat}"
              required>
            </div>
          </div>
        </div>
        <p class="float-right"><small>Du hast bereits ein Konto? <a href="/login">Melde dich an!</a></small></p>
        <br><hr>
        <div class="mt-4">
          <button type="submit" class="btn btn-lg btn-primary btn-block capitialize">{$register}</button>
        </div>
      </form>
    </div>
  </div>
</div>
{include file="footer.tpl"}
