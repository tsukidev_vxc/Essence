{include file="navbar.tpl"}
<div class="container mt-3" style="color: black;">
  <h6 class="text-center bold-text"><span id="LoginResult"></span></h6>
  <div class="card card-signin my-3">
    <div class="card-body">
      <form id="Login_form" class="form-signin" action="#" method="post">
        <div class="form-label-group">
          <input type="text" name="UserName" class="form-control" placeholder="{$username}" required>
        </div>
        <br>
        <div class="form-label-group">
          <input type="password" name="Password" class="form-control" placeholder="{$password}" required>
        </div>
        <br>
        <div class="custom-control custom-checkbox mb-3">
          <input type="checkbox" class="custom-control-input" id="StayLogin" name="StayLogin">
          <label class="custom-control-label" for="StayLogin"><small>{$staylogin}</small></label>
          {if $config['sitesettings:switch']['userregistration']}
          <p class="float-right"><small>Noch kein Konto? <a href="/register">Registiere dich!</a></small></p>
          {/if}
        </div>
        <hr>
        <button class="btn btn-lg btn-primary btn-block capitialize" type="submit">{$login}</button>
      </form>
    </div>
  </div>
</div>
{include file="footer.tpl"}
