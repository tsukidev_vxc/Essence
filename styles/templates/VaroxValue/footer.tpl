</div> <!-- Important DO NOT REMOVE -->

<div class="animated fadeIn slow delay-2s">
  <!-- Edit From Here -->
  <div class="bg-primary mt-5" style="padding-top: 5px; padding-bottom: 5px;">
    <div class="container text-right">
      {if $user->Login == "true"}
      <a href="/admin" class="btn btn-primary btn-rounded">
        Admin Panel
      </a>
      <a href="/logout" class="btn btn-danger btn-rounded">
        Logout
      </a>
      {else}
      <a href="/login" class="btn btn-primary btn-rounded">
        Login
      </a>
      {/if}
    </div>
  </div>
  <section class="px-md-5 py-5 text-center white-text elegant-color z-depth-1">
    <h4>{$config['siteinformation:input']['title']}</h4>
    <h6 class="">Made with <i class="fas fa-heart red-text mx-1"></i> by <a class="white-text" href="https://github.com/leVenour">leVenour</a> and <a class="white-text" href="https://github.com/SGZoey">Zoey</a></h6>
  </section>
</div>
<script type="text/javascript" src="../dependency/mdb/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../dependency/mdb/js/popper.min.js"></script>
<script type="text/javascript" src="../dependency/mdb/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../dependency/mdb/js/mdb.min.js"></script>
