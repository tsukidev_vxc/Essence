<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

$lang = array(
    'short' => 'DE',
    'long' => 'Deutsch',
    'flag' => '<i class="germany flag"></i>',
    'language_creator' => 'Zoey'
);

?>
