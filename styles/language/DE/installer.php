<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

$language_install = array(

  // Welcome Page
  'welcome_title' => 'Willkommen beim Essence Installer!',
  'welcome_text' => 'In den nächsten Schritten muss das System mit einem MySQL Server verbunden werden.<br>
      Um dies zu beginnen bitten wir sie ihren Licence key in das Feld einzugeben.',
  'php_version' => 'PHP-Version',
  'write_perms' => 'Schreibrechte',
  'cache_write_perms' => 'Cache-File created',
  'licence_key_name' => 'Licence Key',
  'licence_key_error' => 'Dieser Key exitiert nicht!',
  'requirements_not_met' => 'Vorraussetzungen nicht erfüllt',
  'license_server_offline_text' => 'Der Lizenzserver ist offline, du kannst ihrgendeine Lizenz eingeben',

  // Database Page
  'database_title' => 'Datenbankeinstellungen',
  'database_text' => 'In diesem schritt muss Essence mit einer MySQL Datenbank verbunden werden. </br>Diese Datenbank muss bereits erstellt sein. Nutze hierfür PHPMYADMIN oder ähnliche Programme. <a href="http://webvaultwiki.com.au/Default.aspx?Page=Create-Mysql-Database-User-Phpmyadmin&NS=&AspxAutoDetectCookieSupport=1">Mehr Infos</a>',
  'database_ip' => 'Datenbank IP',
  'database_port' => 'Datenbank Port',
  'database_username' => 'Datenbank Nutzer',
  'database_password' => 'Datenbank Password',
  'database_name' => 'Datenbank Namen',
  'database_error' => 'Datenbankverbindung fehlgeschlagen',

  // Site_Information Page
  'siteinformation_titletext' => 'Seiteneinstellungen',
  'siteinformation_text' => 'In diesem schritt können Sie die Seiteneinstellungen ändern.',
  'siteinformation_title' => 'Website Titel',
  'siteinformation_keywords' => 'Schlüsselwörter für die Google suche',
  'siteinformation_description' => 'Website beschreibung',

  // Account Page
  'account_titletext' => 'Admin Account Erstellung',
  'account_text' => 'In diesem schritt können Sie sich Ihr erstes Admin Konto erstellen.',
  'account_username' => 'Nutzername',
  'account_email' => 'E-Mail Adresse',
  'account_password' => 'Passwort eingeben',
  'account_passwordrepeat' => 'Passwort wiederholen',

  // Install_Process Page
  'install_title' => 'Installationsprozess',
  'install_text' => 'Dies ist der letzte Schritt vom Installationsprozess, bitte schließe diese Seite nicht bis die Installation abgeschlossen ist.',

  // General
  'next_step' => 'Weiter',
  'license' => 'License Key',
  'license_owner' => 'License Besitzer',
  'compatible' => 'Kompatibel',
  'not_compatible' => 'Nicht Kompatibel'
);
