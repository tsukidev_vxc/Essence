<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

$language_admin = array(
  // Modals
  'username' => 'Nutzername',
  'email' => 'E-Mail Adresse',
  'password' => 'Passwort',
  'save_changes' => 'Änderungen speichern',
  'close' => 'Schließen',

  // Navbar
  'show_menu' => 'Menü anzeigen',
  'hide_menu' => 'Menü verstecken',
  'mainsite' => 'Hauptseite',
  'logout' => 'Ausloggen',
  'lang' => 'Sprache',

  // Sidebar
  'dashboard' => 'Startseite',
  'groups' => 'Gruppen',
  'users' => 'Benutzer',
  'settings' => 'Einstellungen',
  'dashboard' => 'Dashboard',
  'settings' => 'Einstellungen',
  'new_update' => 'Es ist ein neues Update verfügbar:',
  'update_text' => 'Update',
  'main_site' => 'Hauptseite',
  'template' => 'Design',

  // Dashboard
  'dashboard_title' => 'Dashboard',
  'dashboard_text' => '
  Vielen Dank, dass Sie das Website System von Essence nutzen,
  bei Fragen oder Bugs können Sie uns eine Email schreiben,
  wenn Sie eine andere Möglichkeit suchen mit uns in Kontakt zu treten, dann können Sie das gerne auch über Discord tun. ',
  'site_info' => 'Site Info',
  'changelog' => 'Changelog',
  'current_version' => 'Current Version',
  'update_website_text1' => 'Diese Website läuft auf einer alten Version! Bitte update zu unsere neuen Version',
  'update_website_text2' => 'Um dies zu tun bitte lade die Update datei runter und ziehe sie in deinen Root Ordner.
    <br>Daraufhin klicke auf den Update knopf!',
  'website_up_to_date' => 'Die Website ist auf dem neusten Stand!',
  'download' => 'Herunterladen',
  'online_text' => 'Server Status',
  'online' => 'Online',
  'offline' => 'Offline',

  // Settings
  'set' => 'Setzen',
  'on' => 'Aktiviert',
  'off' => 'Deaktiviert',

  // Infos
  'license_owner' => 'Lizenziert für',
  'license_key' => 'Lizenzschlüssel',
  'important_links' => 'Important Links',

  // users
  'stats' => 'Statistiken',
  'add_user' => 'Benutzer hinzufügen / editieren',

  // groups
  'add_group' => 'Gruppe hinzufügen / editieren',
  'total_groups' => 'Total Groups:',
  'newest_group' => 'Newest Group',
  'group_sort' => 'Gruppen Sortierung',
  'group_name' => 'Gruppen Namen',
  'group_color' => 'Gruppen Farbe',
  'group_html' => 'Gruppen HTML',
  'group_html_info' => 'schließe die HTML tags',
  'all_groups' => 'Alle Gruppen',
  'actions' => 'Aktionen',

  //template
  'languages' => 'Sprachen',
  'templates' => 'Templates',
  'online_languages' => 'Alle Sprachen',
  'broken_language' => 'Kaputte Sprachen',
  'lang_info' => 'Falls eine Sprache als Kaputt angezeigt wird, bedeuetet es, dass eine wichtige Datei in dem Sprachorder fehlt!
                  </br>Bitte überprüfe den Ordner auf die nötigen Dateien und lade diese Seite neu!',
  'no_working_lang' => 'Keine Sprache geht gerade!',
  'no_broken_lang' => 'Alle Sprachen gehen gerade! :D',

  // 404
  'site_not_found' => 'Diese Seite wurde nicht gefunden'
);
