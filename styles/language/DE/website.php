<?php
/*
      _____
    |  ___|
    | |__ ___ ___  ___ _ __   ___ ___
    |  __/ __/ __|/ _ \ '_ \ / __/ _ \
    | |__\__ \__ \  __/ | | | (_|  __/
    \____/___/___/\___|_| |_|\___\___|

      ♡ Code by leVenour and Zoey ♡

            ➤ leVenour.at
             ➤ Zooeey.de
*/

$language = array(
  // 404 Page
  'error_404' => 'Die von Ihnen eingegebene Seite wurde nicht gefunden!',

  // Login Page
  'login_title' => 'Einloggen',
  'username' => 'Nutzername',
  'password' => 'Passwort',
  'staylogin' => 'Angemeldet bleiben',
  'login' => 'Einloggen',
  'register' => 'Registrieren',
  'invalid_username_or_password' => 'Ungültiger Nutzername oder Passwort!',
  'success_login' => 'Erfolgreich angemeldet',
  'back_to_mainsite' => 'Zurück zur Hauptseite',

  // Register Page
  'register_title' => 'Registrieren',
  'username' => 'Nutzername',
  'email' => 'E-Mail Adresse',
  'password' => 'Passwort',
  'password_repeat' => 'Passwort wiederholen',
  'register' => 'Registrieren',
  'passwords_not_match' => 'Die Passwörter stimmen nicht überein',
  'username_or_email_already_exists' => 'Dieser Nutzername oder Email ist bereits bei uns registriert',
  'success_register' => 'Erfolgreich registriert',
  'back_to_login' => 'Zurück zum Login'
);
