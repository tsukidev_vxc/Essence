<?php
/* Smarty version 3.1.34-dev-7, created on 2020-03-05 12:45:36
  from 'C:\laragon\www\Essence\core\templates\scripts\admin_scripts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e60f4704da122_88286017',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5f567c3fa414ddefb11e3e905566f48142b9e168' => 
    array (
      0 => 'C:\\laragon\\www\\Essence\\core\\templates\\scripts\\admin_scripts.tpl',
      1 => 1583412335,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e60f4704da122_88286017 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="../dependency/mdb/js/jquery-3.4.1.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../dependency/mdb/js/popper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../dependency/mdb/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../dependency/mdb/js/mdb.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../dependency/mdb/js/addons/datatables.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="../core/js/main.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="https://kit.fontawesome.com/4e8245fcbb.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "md-toast-bottom-right",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    $(document).ready(function () {
        $('select[name=selectChangeGroup]').on('change', function (e) {
            var row = $(this).parent().parent();
            var UID = row.attr('id');
            var UserName = row.find('td')[0].innerText;
            var strGroup = $(this).val();
            $.post('/core/includes/admin/changegroup.inc.php', {
                UserID: UID,
                Group: strGroup
            }).then(() => {
                toastr.success('Der User ' + UserName + ' ist nun ein ' + strGroup + '.');
            });
        });
        $('#settings_form').submit(function (e) {
            var UserName = $('[name=Settings_UserAvatar]').val();
            var UserName = $('[name=Settings_UserName]').val();
            var UserName = $('[name=Settings_UserMail]').val();
            var UserName = $('[name=Settings_Password]').val();
        })
        $('#Addgroup_form').submit(function (e) {
            e.preventDefault();
            var groupSort = $('input[name=GroupSort]').val();
            var groupName = $('input[name=GroupName]').val();
            var groupColor = $('input[name=GroupColor]').val();
            var groupHTML1 = $('input[name=GroupHTML1]').val();
            var groupHTML2 = $('input[name=GroupHTML2').val();
            $.post('/core/includes/admin/addgroup.inc.php', {
                GroupSort: groupSort,
                GroupName: groupName,
                GroupColor: groupColor,
                GroupHTML1: groupHTML1,
                GroupHTML2: groupHTML2
            }).then(response => {
                var json = JSON.parse(JSON.stringify(response));
                $('#addgroup-result').html(json.message);
                if (json.success == "true") {
                    location.reload();
                }
            })
        });
        $('.manageperms').on('click', function (e) {
            e.preventDefault();
            var row = $(this).parent().parent();
            var GID = row.attr('id');
            $('#perms_groupname').html(row.find('td')[1].innerText);
            $.post('/core/includes/admin/getgroup.inc.php', {
                GroupID: GID
            }).then(response => {
                var json = JSON.parse(JSON.stringify(response));
                var result = '<div class="list-group list-group-flush">';
                if (json.permissions.length <= 0) {
                    toastr.error('Es wurden keine Permissions in der Datenbank gefunden');
                    return;
                }
                $('#perms_modal').modal('show');
                $.each(json.permissions, (obj) => {
                    var jsonObj = json.permissions[obj];
                    result += '<div class="input-group">' +
                        '<div class="switch">' +
                        '<label>' +
                        '<a class="pl-1">' + jsonObj.desc + '</a>:' +
                        '<input type="checkbox" name="' + jsonObj.permission + '" ' + (jsonObj.have ? 'checked' : '') + ' ' + (jsonObj.disabled ? 'disabled' : '') + '>' +
                        '<span class="lever"></span>' +
                        '</label>' +
                        '</div></div>' + (jsonObj.permission == '*' ? '<hr>' : '');

                });
                result += '</div>';
                $('#perms_content').html(result);
                $('#perms_content').find('input').each(function () {
                    $(this).on('change', function (e) {
                        var Permission = $(this).attr('name');
                        $.post('/core/includes/admin/togglepermission.inc.php', {
                            GroupID: GID,
                            Permission: Permission,
                            Set: Boolean($(this).is(':checked'))
                        }).then((response) => {
                            if ($(this).is(':checked')) {
                                toastr.success('Permission ' + Permission + ' hinzugefügt', null, {
                                    timeOut: 1000
                                });
                            } else {
                                toastr.warning('Permission ' + Permission + ' entfernt', null, {
                                    timeOut: 1000
                                });
                            }
                        });
                    });
                });
            });
        });
        $('.showusers').on('click', function (e) {
            e.preventDefault();
            var row = $(this).parent().parent();
            var GID = row.attr('id');
            $('#groupmembers_groupname').html(row.find('td')[1].innerText);
            $.post('/core/includes/admin/getgroup.inc.php', {
                GroupID: GID
            }).then(response => {
                var json = JSON.parse(JSON.stringify(response));
                var result = '<div class="list-group list-group-flush">';
                if (json.users.length <= 0) {
                    toastr.error('In dieser Gruppe befinden sich keine Nutzer');
                    return;
                }
                $('#groupmembers_modal').modal('show');
                $.each(json.users, (obj) => {
                    var username = json.users[obj];
                    result += '<div class="input-group">' +
                        '<label>' +
                        '<button name="' + username + '" class="btn admin-bg-red btn-sm white-text"><i class="fas fa-trash"></i></button>' +
                        '<a class="pl-5">' + username + '</a>' +
                        '</label>' +
                        '</div>';
                });
                result += '</div>';
                $('#groupmembers_content').html(result);
                $('#groupmembers_content').find('button').each(function () {
                    $(this).on('click', function (e) {
                        var UserName = $(this).attr('name');
                        e.preventDefault();
                        $.post('/core/includes/admin/removeuserfromgroup.inc.php', {
                            UserName: UserName
                        }).then(() => {
                            toastr.error('Der User ' + UserName + ' ist kein ' + row.find('td')[0].innerText + ' mehr.');
                            $(this).parent().parent().remove();
                        });
                    });
                });
            });
        });
        $('.deletegroup').on('click', function (e) {
            e.preventDefault();
            var row = $(this).parent().parent();
            var GID = row.attr('id');
            $.post('/core/includes/admin/deletegroup.inc.php', {
                GID: GID
            }).then(response => {
                location.reload();
            });
        });
        $('.editgroup').on('click', function (e) {
            e.preventDefault();
            var row = $(this).parent().parent();
            var GID = row.attr('id');
            var GroupSort = row.find('td')[0].innerText;
            var GroupName = row.find('td')[1].innerText;
            var GroupColor = row.find('td')[3].innerHTML;
            var GroupHTML1 = row.find('td')[2].innerHTML;
            var GroupHTML2 = row.find('td')[4].innerHTML;
            $('[name=GroupSort]').val(GroupSort);
            $('[name=GroupName]').val(GroupName);
            $('[name=GroupColor]').val(GroupColor);
            $('[name=GroupHTML1]').val(GroupHTML1);
            $('[name=GroupHTML2]').val(GroupHTML2);
        });
        $('#users_list').DataTable();
        $('#groups_list').DataTable();
        $('input').each(function () {
            $(this).trigger('focusin');
            $(this).trigger('blur');
        });
        setInterval(function () {
            $('#menu-toggle').html(($('#wrapper').hasClass('toggled') ? "<?php echo $_smarty_tpl->tpl_vars['show_menu']->value;?>
" : "<?php echo $_smarty_tpl->tpl_vars['hide_menu']->value;?>
"));
        }, 100);
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    });

    function setConfig(key_name, value) {
        $.post('/core/includes/admin/setconfig.inc.php', {
            key_name: key_name,
            value: value
        }).then(function (response) {
        })
    }

    function setDefault(TemplateID) {
        $.post('/core/includes/admin/setdefaulttemplate.inc.php', {
            TemplateID: TemplateID
        }).then(response => {
            location.reload();
        });
    }

    function changeTemplateState(TemplateID, NewState) {
        $.post('/core/includes/admin/changetemplatestate.inc.php', {
            TemplateID: TemplateID,
            NewState: NewState
        }).then(response => {
            location.reload();
        });
    }
<?php echo '</script'; ?>
><?php }
}
