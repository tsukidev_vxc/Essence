<?php
/* Smarty version 3.1.34-dev-7, created on 2020-03-04 10:58:05
  from 'C:\laragon\www\Essence\core\templates\admin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e5f89bd228745_79476667',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f848405013bfd9efb60d4ced21df9a76500b10a8' => 
    array (
      0 => 'C:\\laragon\\www\\Essence\\core\\templates\\admin.tpl',
      1 => 1583319484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/css/light.tpl' => 1,
    'file:admin/css/dark.tpl' => 1,
    'file:scripts/admin_scripts.tpl' => 1,
    'file:admin/dashboard.tpl' => 1,
    'file:admin/groups.tpl' => 1,
    'file:admin/settings.tpl' => 1,
    'file:admin/information.tpl' => 1,
    'file:admin/update.tpl' => 1,
    'file:admin/users.tpl' => 1,
    'file:admin/templates.tpl' => 1,
    'file:admin/404.tpl' => 1,
    'file:admin/Modals.tpl' => 1,
  ),
),false)) {
function content_5e5f89bd228745_79476667 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['theme']->value == "light") {
$_smarty_tpl->_subTemplateRender("file:admin/css/light.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} else {
$_smarty_tpl->_subTemplateRender("file:admin/css/dark.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
$_smarty_tpl->_subTemplateRender("file:scripts/admin_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="d-flex" id="wrapper">
  <center>
    <div class="bg-dark" id="sidebar-wrapper">
      <div class="sidebar-heading white-text">
        <a href="/admin"><img height="75px" src="<?php echo $_smarty_tpl->tpl_vars['config']->value['image:upload']['logo'];?>
"></img></a>
      </div>
      <div class="list-group list-group-flush">
        <a href="/admin" class="<?php if ($_smarty_tpl->tpl_vars['Page']->value == "dashboard") {?>active<?php }?> list-group-item list-group-item-action bg-dark
          white-text"><?php echo $_smarty_tpl->tpl_vars['dashboard']->value;?>
</a>
        <a href="/admin/settings" class="<?php if ($_smarty_tpl->tpl_vars['Page']->value == "settings") {?>active<?php }?> list-group-item list-group-item-action
          bg-dark white-text"><?php echo $_smarty_tpl->tpl_vars['settings']->value;?>
</a>
        <a href="/admin/users" class="<?php if ($_smarty_tpl->tpl_vars['Page']->value == "users") {?>active<?php }?> list-group-item list-group-item-action bg-dark
          white-text"><?php echo $_smarty_tpl->tpl_vars['users']->value;?>
</a>
        <a href="/admin/groups" class="<?php if ($_smarty_tpl->tpl_vars['Page']->value == "groups") {?>active<?php }?> list-group-item list-group-item-action bg-dark
          white-text"><?php echo $_smarty_tpl->tpl_vars['groups']->value;?>
</a>
        <a href="/admin/templates" class="<?php if ($_smarty_tpl->tpl_vars['Page']->value == "templates") {?>active<?php }?> list-group-item list-group-item-action bg-dark
          white-text"><?php echo $_smarty_tpl->tpl_vars['template']->value;?>
</a>
        <a href="/admin/update" class="<?php if ($_smarty_tpl->tpl_vars['Page']->value == "update") {?>active<?php }?> list-group-item list-group-item-action bg-dark
          white-text"><?php echo $_smarty_tpl->tpl_vars['update_text']->value;?>
</a>
      </div>
    </div>
  </center>
  <div id="page-content-wrapper" class="admin-bg">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <button class="btn btn-sm white-text" id="menu-toggle"><?php echo $_smarty_tpl->tpl_vars['show_menu']->value;?>
</button>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <div class="dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false"><?php echo $_smarty_tpl->tpl_vars['user']->value->UserName;?>
</a>
              <!--Menu-->
              <div class="dropdown-menu dropdown-primary">
                <a class="dropdown-item" href="/settings"><?php echo $_smarty_tpl->tpl_vars['settings']->value;?>
</a>
                <a class="dropdown-item" href="/logout"><?php echo $_smarty_tpl->tpl_vars['logout']->value;?>
</a>
              </div>
            </div>
          </li>
          <li class="nav-item"><a class="btn white-text admin-bg-blue btn-sm btn-rounded" href="/"><?php echo $_smarty_tpl->tpl_vars['main_site']->value;?>
</a></li>
          <li class="nav-item">
            <div class="dropdown">
              <a class="btn white-text admin-bg-red btn-sm btn-rounded" data-toggle="dropdown"><?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
</a>
              <!--Menu-->
              <div class="dropdown-menu dropdown-primary">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['available_languages']->value, 'i', false, 'myId');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['i']->value) {
?>
                <a href="/admin?lang=<?php echo $_smarty_tpl->tpl_vars['i']->value['short'];?>
&link=<?php echo $_smarty_tpl->tpl_vars['Link']->value;?>
" class="dropdown-item" href="?lang=DE"><?php echo $_smarty_tpl->tpl_vars['i']->value['flag'];?>
 <?php echo $_smarty_tpl->tpl_vars['i']->value['long'];?>
</a>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </div>
            </div>
          </li>
          <?php if ($_smarty_tpl->tpl_vars['theme']->value == "light") {?>
          <li class="nav-item"><a href="/admin?theme=dark&link=<?php echo $_smarty_tpl->tpl_vars['Link']->value;?>
"
              class="btn btn-purple btn-rounded btn-sm px-3 changeTheme"><i class="fas fa-moon"></i></a></li>
          <?php } else { ?>
          <li class="nav-item"><a href="/admin?theme=light&link=<?php echo $_smarty_tpl->tpl_vars['Link']->value;?>
"
              class="btn btn-primary btn-rounded btn-sm px-3 changeTheme"><i class="fas fa-sun"></i></a></li>
          <?php }?>
          <li class="nav-item"><a href="/admin/information"
              class="btn white-text admin-bg-blue btn-rounded btn-sm px-3"><i class="fas fa-question"></i></a></li>
        </ul>
      </div>
    </nav>

    <?php if ($_smarty_tpl->tpl_vars['config']->value['sitesettings:switch']['maintenance']) {?>
    <div class="admin-bg-red" style="text-align: center; color: white">
      Die Website befindet sich gerade im Wartungsmodus! Normale User werden sie nicht betrachten können! <a
        class="btn white-text admin-bg-purple btn-rounded btn-sm" href="/admin/settings">Zu den Einstellungen</a>
    </div>
    <?php }?>

    <div class="container-fluid mt-4 animated fadeIn">
      <?php if ($_smarty_tpl->tpl_vars['Page']->value == "dashboard") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/dashboard.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } elseif ($_smarty_tpl->tpl_vars['Page']->value == "groups") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/groups.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } elseif ($_smarty_tpl->tpl_vars['Page']->value == "settings") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/settings.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } elseif ($_smarty_tpl->tpl_vars['Page']->value == "information") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/information.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } elseif ($_smarty_tpl->tpl_vars['Page']->value == "update") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/update.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } elseif ($_smarty_tpl->tpl_vars['Page']->value == "users") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/users.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } elseif ($_smarty_tpl->tpl_vars['Page']->value == "templates") {?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/templates.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php } else { ?>

      <?php $_smarty_tpl->_subTemplateRender("file:admin/404.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php }?>
    </div>
  </div>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:admin/Modals.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
