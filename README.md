# Essence

Essence is a custom Website System coded by leVenour and Zoey. It is for private use. Please contact us under
[admin@levenour.at](mailto:admin@levenour.at)

[zoey@zooeey.de](mailto:zoey@zooeey.de)

## Installation

Download using our download [server](https://download.zooeey.de/essence). Once there please login using your username and password.
If you forgot your information please contact us.

## Usage

Pull into your main /www folder.
Open the Website. You will be redirected to our installer. Fill out the Information needed and the Website will generate itself.

## Contributing
In case you want to join us please contact us per email or discord.

## Contact
Email: [admin@levenour.at](mailto:admin@levenour.at) & [zoey@zooeey.de](mailto:zoey@zooeey.de)

Discord: leVenour#1997 & Zoey#1960

# TODO

- Installer 	✓

- User System  ✓

- Gruppe System ✓

- Licence Server 	✓

- Basis Website Info ✘

- Template System ✘

- Update System  ✘


## @leVenour
- Installer 	✓

- Config System 	✓

  ```
  class ExampleConfig extends Config {
    function __construct(){
      parent::__construct('PATH-FROM-ROOT', 'FILENAME.php');
    }
  }
  ```

  ```
  $exampleConfig = new ExampleConfig;
  // Get an entry from config.
  $exampleConfig->get('THE-KEY'); // Check first if the entry exists

  // Set or Replace an entry to config.
  $exampleConfig->set('THE-KEY', 'THE-VALUE');

  // Delete an entry from Config
  $exampleConfig->remove('THE-KEY');
  ```

- User System

  ```
  $user = new User($UserName, $Password /** if you want to login if not then leave it */);
  // or
  $user = User::getUserByID($UserID);
  
  // Set Info and fetch it to database
  $user->setInfo($key, $value);
  // Get Info
  $user->getInfo($key); // Return the info or an empty string if the info not exists
  
  // Get or sets the Group Object
  $user->Group;
  
  $user->Group->hasPermission(/** Permission node */)

  ```

  - Login (-> Mit Cookies (um angemeldet zu bleieben) sonst sessions) 	✓

  - Register ✓

- Gruppen System

  - Gruppe hinzufügen ✓

  - Gruppen editieren ✓

-Rechte System

  - erweiterbares Perm System ✓



## @Zoey
- Licence Server 	✓

- Template System ✘

- Language System 	✓

- Update System ✘

- Route System 	✓



## Folder Struktur
core/ --> Init file, /generators, /functions, /templates (für installer und admin panel), /logs

dependency/ --> Smarty, CKEditor, CKFinder, Javascript Libs

pages/ --> Index Folder for pages

styles/ --> /templates,/language --> Templates and Language Data

uploads --> /assets and user uploads



## Database Struktur
ec_users --> User_ID, User_Name, User_Password, User_Email

ec_user_info --> UInfo_ID, UInfo_UserID, UInfo_Desc, UInfo_Key, UInfo_Value

ec_groups --> Group_ID, Group_Sort, Group_Name, Group_Color, Group_HTML, Group_HTML_2

ec_perms_list --> PermL_ID, PermL_Name, PermL_Desc

ec_perms --> Perm_ID, Perm_Group, Perm_PermL

ec_templates --> Template_ID, Template_Name, Template_Preview, Template_Color, Template_Active, Template_Standard

ec_uploads --> Upload_ID, Upload_File_Link, Upload_User_ID, Upload_IP

ec_language --> Language_ID, Langauge_Name, Langauge_Link

ec_route --> Route_ID, Route_User, Route_Link
